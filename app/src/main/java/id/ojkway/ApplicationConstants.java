package id.ojkway;

public interface ApplicationConstants {
	String SERVER_API = "http://ojkway.ojk.go.id/";
	String SERVER_ASSETS = "http://ojkway.ojk.go.id/";
	String SERVER_BASE_URL = "http://ojkway.ojk.go.id/";
	static final String GOOGLE_PROJ_ID = "876640292227";
	static final String MSG_KEY = "m";

}
