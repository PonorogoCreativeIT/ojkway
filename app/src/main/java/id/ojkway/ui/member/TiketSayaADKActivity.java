package id.ojkway.ui.member;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.DetailAdkAdapter;
import id.ojkway.model.DetailAdkModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;
import id.ojkway.widgets.MonseratTextView;

public class TiketSayaADKActivity extends AppCompatActivity {

    View viewMengatur;
    View viewMengawasi;
    View viewMelindungi;
    View viewUntukIndustri;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    MonseratTextView title;
    @BindView(R.id.viewList)
    ListView viewList;
    @BindView(R.id.notfound)
    View notfound;


    private long enqueue;
    private DownloadManager dm;

    DetailAdkAdapter detailAdkAdapter;
    List<DetailAdkModel> listData = new ArrayList<DetailAdkModel>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailadk);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();
        AndroidNetworking.initialize(this);

        header_not_null = getLayoutInflater().inflate(R.layout.header_adk, null);
        header_null = getLayoutInflater().inflate(R.layout.header_adk_notfound, null);

        title.setText("Tiket Saya");
        viewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DetailAdkModel getData = listData.get(i - 1);
                Bundle setData = new Bundle();
                setData.putString("id", getData.getid());
                startActivity(new Intent(TiketSayaADKActivity.this, DetailTiketAdkActivity.class).putExtras(setData));
                /*Log.v("url : ", ApplicationConstants.SERVER_ASSETS + "images/file_sharing/" + getData.geturl());
                dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(
                        Uri.parse(ApplicationConstants.SERVER_ASSETS + "images/file_sharing/" + getData.geturl().replace(" ","%20")));
                enqueue = dm.enqueue(request);
                Toast.makeText(DetailADKActivity.this, "Proses download sudah dimulai", Toast.LENGTH_SHORT).show();*/
            }
        });

        detailAdkAdapter = new DetailAdkAdapter(this, listData);
        viewList.setAdapter(detailAdkAdapter);

        /*ANRequest.PostRequestBuilder load = AndroidNetworking.post("");
        load.addBodyParameter("[]","");*/




        loadData();
    }


    @Override
    protected void onResume(){
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    View header_not_null;
    View header_null;
    void updateView(){
        if(listData.size() > 0){
            viewMengatur = header_not_null.findViewById(R.id.viewMengatur);
            viewMengawasi = header_not_null.findViewById(R.id.viewMengawasi);
            viewMelindungi = header_not_null.findViewById(R.id.viewMelindungi);
            viewUntukIndustri = header_not_null.findViewById(R.id.viewUntukIndustri);

            viewList.addHeaderView(header_not_null);
            viewList.removeHeaderView(header_null);
            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    loadAnimation();
                }
            };
            handler.postDelayed(r, 500);


            notfound.setVisibility(View.GONE);
            viewList.setVisibility(View.VISIBLE);
        }else{
            viewMengatur = header_null.findViewById(R.id.viewMengatur);
            viewMengawasi = header_null.findViewById(R.id.viewMengawasi);
            viewMelindungi = header_null.findViewById(R.id.viewMelindungi);
            viewUntukIndustri = header_null.findViewById(R.id.viewUntukIndustri);

            viewList.addHeaderView(header_null);
            viewList.removeHeaderView(header_not_null);
            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    loadAnimation();
                }
            };
            handler.postDelayed(r, 500);
            notfound.setVisibility(View.GONE);
            viewList.setVisibility(View.VISIBLE);
        }
    }

    String icon;

    void loadData(){
        listData.clear();
        Log.v("url", ApplicationConstants.SERVER_API + "api/v1/adk?token=" + utils.getDataSession(this, "sessionTOKEN"));
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/adk?token=" + utils.getDataSession(this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("response", response.toString());
                        try{
                            JSONArray ticket = response.getJSONObject("data").getJSONArray("my_ticket");
                            for(int i = 0 ; i < ticket.length() ; i++){
                                JSONObject c = ticket.getJSONObject(i);
                                DetailAdkModel setData = new DetailAdkModel();
                                setData.setid(c.getString("id"));
                                setData.setnama(c.getJSONObject("untuk").getString("first_name") + " " + c.getJSONObject("dari").getString("last_name"));
                                setData.setimage(c.getJSONObject("untuk").getString("photo_profile"));
                                setData.settext(c.getString("text"));
                                listData.add(setData);
                            }
                            detailAdkAdapter.notifyDataSetChanged();
                            updateView();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        utils.logLargeString("", anError.getErrorBody().toString());
                    }
                });
    }

    void loadAnimation(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewMengatur.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.SlideInLeft)
                                .duration(500)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        viewMengawasi.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        YoYo.with(Techniques.SlideInLeft)
                                                .duration(500)
                                                .withListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {
                                                        viewMelindungi.setVisibility(View.VISIBLE);                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        YoYo.with(Techniques.FadeInUp)
                                                                .duration(500)
                                                                .withListener(new Animator.AnimatorListener() {
                                                                    @Override
                                                                    public void onAnimationStart(Animator animation) {
                                                                        viewUntukIndustri.setVisibility(View.VISIBLE);                                                    }

                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationCancel(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationRepeat(Animator animation) {

                                                                    }
                                                                })
                                                                .playOn(viewUntukIndustri);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                })
                                                .playOn(viewMelindungi);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(viewMengawasi);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewMengatur);
    }

}
