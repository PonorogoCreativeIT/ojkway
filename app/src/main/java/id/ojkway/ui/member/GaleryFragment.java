package id.ojkway.ui.member;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.gigamole.navigationtabstrip.NavigationTabStrip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.utils.utils;
import id.ojkway.widgets.WrapContentViewPager;

/**
 * Created by Admin on 9/4/2016.
 */
public class GaleryFragment extends android.support.v4.app.Fragment {

    @BindView(R.id.tabs)
    NavigationTabStrip tabs;
    @BindView(R.id.viewPager)
    ViewPager viewpager;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_galery, container, false);
        ButterKnife.bind(this, v);
        getAllFragment();
        return v;
    }

    public void getAllFragment(){
        setupViewPager(viewpager);
        tabs.setViewPager(viewpager);
    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getChildFragmentManager());

        ImageFragment imageFragment = new ImageFragment();

        VideoFragment videoFragment = new VideoFragment();

        adapter.addFragment(imageFragment, "imageFragment");
        adapter.addFragment(videoFragment, "videoFragment");

        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(5);
    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<android.support.v4.app.Fragment> mFragments = new ArrayList<android.support.v4.app.Fragment>();
        private final List<String> mFragmentTitles = new ArrayList<String>();

        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(android.support.v4.app.Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }
}
