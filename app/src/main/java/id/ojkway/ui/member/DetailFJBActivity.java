package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.CommentFJBAdapter;
import id.ojkway.model.CommentFJBModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;
import id.ojkway.widgets.MonseratTextView;
import id.ojkway.widgets.WrapContentListView;

public class DetailFJBActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    MonseratTextView title;
    @BindView(R.id.slideshow)
    SliderLayout slideshow;
    @BindView(R.id.custom_indicator)
    PagerIndicator custom_indicator;
    @BindView(R.id.judul)
    MonseratTextView judul;
    @BindView(R.id.tanggal)
    MonseratTextView tanggal;
    @BindView(R.id.deskripsi)
    MonseratTextView deskripsi;
    @BindView(R.id.profile)
    CircleImageView profile;
    @BindView(R.id.nama_user)
    MonseratTextView nama_user;
    @BindView(R.id.etComment)
    MonseratEditText etComment;
    @BindView(R.id.listComment)
    WrapContentListView listComment;
    @BindView(R.id.btnEditThread)
    View btnEditThread;

    CommentFJBAdapter commentfjbadapter;
    List<CommentFJBModel> listDataComment = new ArrayList<>();

    String id;
    Bundle data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_fjb);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        AndroidNetworking.initialize(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();

        commentfjbadapter = new CommentFJBAdapter(this, listDataComment);
        listComment.setAdapter(commentfjbadapter);

        data = getIntent().getExtras();
        id = data.getString("id");
        title.setText(data.getString("nama"));

    }

    @Override
    protected void onResume(){
        super.onResume();
        loadData();
    }

    @OnClick(R.id.btnEditThread)
    public void btnEditThread(){
        Bundle setDataInte = new Bundle();
        setDataInte.putString("id", id);
        startActivity(new Intent(this, EditFJBActivity.class).putExtras(setDataInte));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    void loadData(){
        final MaterialDialog mDIalog = utils.showDialog(this, "Please wait");
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/fjb/thread/" + id)
                .addBodyParameter("token", utils.getDataSession(this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mDIalog.dismiss();
                        try{
                            if(response.getString("status").equals("true")){
                                judul.setText(response.getJSONObject("data").getJSONObject("thread").getString("title"));
                                tanggal.setText(utils.getTimeAgo(response.getJSONObject("data").getJSONObject("thread").getString("created_at")));
                                deskripsi.setText(Html.fromHtml(response.getJSONObject("data").getJSONObject("thread").getString("description")));
                                nama_user.setText(response.getJSONObject("data").getJSONObject("thread").getJSONObject("seller").getString("first_name") + " " + response.getJSONObject("data").getJSONObject("thread").getJSONObject("seller").getString("last_name"));

                                if(response.getJSONObject("data").getJSONObject("thread").getJSONObject("seller").getString("id").equals(utils.getDataSession(DetailFJBActivity.this, "sessionID"))){
                                    btnEditThread.setVisibility(View.VISIBLE);
                                }else{
                                    btnEditThread.setVisibility(View.GONE);
                                }

                                Picasso.with(DetailFJBActivity.this)
                                        .load(ApplicationConstants.SERVER_ASSETS + response.getJSONObject("data").getJSONObject("thread").getJSONObject("seller").getString("photo_profile"))
                                        .into(profile);

                                JSONArray images = response.getJSONObject("data").getJSONObject("thread").getJSONArray("images");
                                for(int i = 0; i < images.length(); i++){
                                    JSONObject c = images.getJSONObject(i);
                                    Log.v("url : ", ApplicationConstants.SERVER_ASSETS + "images/commerce/" + c.getString("filename"));
                                    DefaultSliderView defaultSliderView = new DefaultSliderView(DetailFJBActivity.this);
                                    defaultSliderView
                                            .image(ApplicationConstants.SERVER_ASSETS + "images/commerce/" + c.getString("filename"))
                                            .setScaleType(BaseSliderView.ScaleType.Fit);

                                    defaultSliderView.bundle(new Bundle());
                                    defaultSliderView.getBundle()
                                            .putString("extra","");

                                    slideshow.addSlider(defaultSliderView);
                                    slideshow.setCustomIndicator(custom_indicator);
                                    slideshow.setDuration(10000);
                                }

                                JSONArray comments = response.getJSONObject("data").getJSONArray("comments");
                                for(int i = 0; i < comments.length(); i++){
                                    JSONObject c = comments.getJSONObject(i);
                                    CommentFJBModel setData = new CommentFJBModel();
                                    setData.setnama(c.getJSONObject("user").getString("first_name") + " " + c.getJSONObject("user").getString("last_name"));
                                    setData.setcomment(c.getString("text"));
                                    setData.setimageprofile(c.getJSONObject("user").getString("photo_profile"));
                                    listDataComment.add(setData);
                                }
                                commentfjbadapter.notifyDataSetChanged();
                            }else{
                                finish();
                                Toast.makeText(DetailFJBActivity.this, "Pengambilan data gagal", Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        mDIalog.dismiss();
                        finish();
                        Toast.makeText(DetailFJBActivity.this, "Pengambilan data gagal", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    String comment = "";
    @OnClick(R.id.btnComment)
    public void btnComment(){
        if(etComment.length() > 2){
            comment = etComment.getText().toString();
            commentNow();
        }else{
            Toast.makeText(this, "Minimal 3 karakter.", Toast.LENGTH_SHORT).show();
        }
    }

    void commentNow(){
        final MaterialDialog mmDialog = utils.showDialog(this, "Please wait");
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/fjb/thread/comment/" + id)
                .addBodyParameter("token", utils.getDataSession(this, "sessionTOKEN"))
                .addBodyParameter("comment_text", comment)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mmDialog.dismiss();
                        try{
                            if(response.getString("status").equals("true")){
                                etComment.setText("");
                                CommentFJBModel setData = new CommentFJBModel();
                                setData.setnama(response.getJSONObject("data").getJSONObject("comment").getJSONObject("user").getString("first_name") + " " + response.getJSONObject("data").getJSONObject("comment").getJSONObject("user").getString("last_name"));
                                setData.setcomment(response.getJSONObject("data").getJSONObject("comment").getString("body"));
                                setData.setimageprofile(response.getJSONObject("data").getJSONObject("comment").getJSONObject("user").getString("gambar"));
                                listDataComment.add(setData);

                            commentfjbadapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        mmDialog.dismiss();
                    }
                });
    }
}
