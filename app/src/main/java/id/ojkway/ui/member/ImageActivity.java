package id.ojkway.ui.member;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ojkway.R;
import uk.co.senab.photoview.PhotoViewAttacher;

public class ImageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewImage)
    ImageView viewImage;
    @BindView(R.id.viewJudul)
    TextView viewJudul;

    Bundle data;
    PhotoViewAttacher mAttacher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        data = getIntent().getExtras();
        Glide.with(this)
                .load(data.getString("url"))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewImage);
        mAttacher = new PhotoViewAttacher(viewImage);
        mAttacher.update();
        mAttacher.setZoomable(true);
        mAttacher.update();
        mAttacher.setScaleType(ImageView.ScaleType.FIT_CENTER);
        mAttacher.update();
        viewJudul.setText(data.getString("judul"));

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
