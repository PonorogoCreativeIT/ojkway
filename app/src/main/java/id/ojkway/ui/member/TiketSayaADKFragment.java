package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.ADKAdapter;
import id.ojkway.adapter.DetailAdkAdapter;
import id.ojkway.adapter.TiketSayaAdapter;
import id.ojkway.model.ADKModel;
import id.ojkway.model.DetailAdkModel;
import id.ojkway.utils.utils;

/**
 * Created by ponorogocreativeit on 21/09/16.
 */

public class TiketSayaADKFragment extends Fragment {

    @BindView(R.id.viewADk)
    ListView viewADk;

    List<DetailAdkModel> listData = new ArrayList<DetailAdkModel>();
    TiketSayaAdapter adkAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tiketsayaadk, container, false);
        ButterKnife.bind(this, view);

        adkAdapter = new TiketSayaAdapter(getActivity(), listData);
        viewADk.setAdapter(adkAdapter);
        viewADk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DetailAdkModel getData = listData.get(i);
                Bundle setDataIntent = new Bundle();
                setDataIntent.putString("id", getData.getid());
                setDataIntent.putString("nama", getData.getnama());
                startActivity(new Intent(getActivity(), DetailADKActivity.class).putExtras(setDataIntent));
            }
        });
        loadData();
        return view;
    }

    int page = 1;
    void loadData(){
        //Log.v("url", ApplicationConstants.SERVER_API + "api/v1/gallery/image/" + page + "?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"));
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/adk?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.logLargeString(response.toString(), "response");
                        try{
                            JSONArray ticket = response.getJSONObject("data").getJSONArray("my_ticket");
                            for(int i = 0 ; i < ticket.length() ; i++){
                                JSONObject c = ticket.getJSONObject(i);
                                DetailAdkModel setData = new DetailAdkModel();
                                setData.setid(c.getString("id"));
                                setData.setnama(c.getJSONObject("dari").getString("first_name") + " " + c.getJSONObject("dari").getString("last_name"));
                                setData.setimage(c.getJSONObject("dari").getString("photo_profile"));
                                setData.settext(c.getString("text"));
                                listData.add(setData);
                            }

                            adkAdapter.notifyDataSetChanged();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        utils.logLargeString(anError.getErrorBody(), "error");
                    }
                });

    }

}
