package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.like.LikeButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.CommentFJBAdapter;
import id.ojkway.model.CommentFJBModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;
import id.ojkway.widgets.MonseratTextView;
import id.ojkway.widgets.WrapContentListView;

public class CommentActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    MonseratTextView title;

    @BindView(R.id.viewProfile)
    CircleImageView viewProfile;
    @BindView(R.id.viewImage)
    ImageView viewImage;
    @BindView(R.id.viewNama)
    MonseratTextView viewNama;
    @BindView(R.id.viewTanggal)
    MonseratTextView viewTanggal;
    @BindView(R.id.viewDeskripsi)
    MonseratTextView viewDeskripsi;
    @BindView(R.id.viewLike)
    MonseratTextView viewLike;
    @BindView(R.id.viewComment)
    MonseratTextView viewComment;
    @BindView(R.id.like)
    LikeButton like;
    @BindView(R.id.layoutAtas)
    LinearLayout layoutAtas;
    @BindView(R.id.layoutTanggal)
    LinearLayout layoutTanggal;
    @BindView(R.id.comment)
    WrapContentListView comment;
    @BindView(R.id.etComment)
    MonseratEditText etComment;

    Bundle data;
    String id;

    CommentFJBAdapter commentfjbadapter;
    List<CommentFJBModel> listDataComment = new ArrayList<>();

    @BindView(R.id.scroll)
    View scroll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();
        AndroidNetworking.initialize(this);
        data = getIntent().getExtras();
        id = data.getString("id");

        commentfjbadapter = new CommentFJBAdapter(this, listDataComment);
        comment.setAdapter(commentfjbadapter);



        loadData();
    }

    String url = "";
    @OnClick(R.id.viewImage)
    public void viewImage(){
        Bundle data = new Bundle();
        data.putString("url", url);
        data.putString("judul", "");
        startActivity(new Intent(this, ImageActivity.class).putExtras(data));
    }

    void loadData(){
        listDataComment.clear();
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/timeline/" + id + "?token=" + utils.getDataSession(this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            Glide.with(CommentActivity.this)
                                    .load(ApplicationConstants.SERVER_ASSETS  + response.getJSONObject("data").getJSONObject("post").getJSONObject("user").getString("photo_profile"))
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(viewProfile);

                            if(response.getJSONObject("data").getJSONObject("post").getString("is_liked").equals("0")){
                                like.setLiked(false);
                            }else{
                                like.setLiked(true);
                            }

                            if(response.getJSONObject("data").getJSONObject("post").has("image")) {
                                if (response.getJSONObject("data").getJSONObject("post").getString("image").equals("") || response.getJSONObject("data").getJSONObject("post").getString("image").equals("null")) {
                                    viewImage.getLayoutParams().height = 60;


                                    LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
                                            utils.getDP(CommentActivity.this, 50), utils.getDP(CommentActivity.this, 50));
                                    layoutParam.setMargins(utils.getDP(CommentActivity.this, 10), utils.getDP(CommentActivity.this, 10), 0, 0);

                                    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                    layoutParams.setMargins(utils.getDP(CommentActivity.this, 10), utils.getDP(CommentActivity.this, 10), 0, 0);

                                    LinearLayout.LayoutParams layoutParamss = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                    layoutParamss.setMargins(0, utils.getDP(CommentActivity.this, 10), 0, 0);

                                    viewNama.setLayoutParams(layoutParams);
                                    viewProfile.setLayoutParams(layoutParam);
                                    layoutTanggal.setLayoutParams(layoutParamss);
                                    layoutAtas.setGravity(Gravity.CENTER_VERTICAL);
                                    viewImage.setVisibility(View.VISIBLE);
                                } else {
                                    viewImage.setVisibility(View.VISIBLE);
                                    Glide.with(CommentActivity.this)
                                            .load(ApplicationConstants.SERVER_ASSETS + "images/user_status/" + response.getJSONObject("data").getJSONObject("post").getString("image"))
                                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                                            .into(viewImage);

                                    url = ApplicationConstants.SERVER_ASSETS + "images/user_status/" + response.getJSONObject("data").getJSONObject("post").getString("image");
                                }
                            }else{
                                viewImage.getLayoutParams().height = 60;


                                LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
                                        utils.getDP(CommentActivity.this, 50), utils.getDP(CommentActivity.this, 50));
                                layoutParam.setMargins(utils.getDP(CommentActivity.this, 10), utils.getDP(CommentActivity.this, 10), 0, 0);

                                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
                                layoutParams.setMargins(utils.getDP(CommentActivity.this, 10), utils.getDP(CommentActivity.this, 10), 0, 0);

                                LinearLayout.LayoutParams layoutParamss = new LinearLayout.LayoutParams(
                                        LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                layoutParamss.setMargins(0, utils.getDP(CommentActivity.this, 10), 0, 0);

                                viewNama.setLayoutParams(layoutParams);
                                viewProfile.setLayoutParams(layoutParam);
                                layoutTanggal.setLayoutParams(layoutParamss);
                                layoutAtas.setGravity(Gravity.CENTER_VERTICAL);
                                viewImage.setVisibility(View.VISIBLE);
                            }
                            viewNama.setText(response.getJSONObject("data").getJSONObject("post").getJSONObject("user").getString("first_name") + " " + response.getJSONObject("data").getJSONObject("post").getJSONObject("user").getString("last_name"));
                            viewTanggal.setText(utils.getTimeAgo(response.getJSONObject("data").getJSONObject("post").getString("timestamp")));
                            viewDeskripsi.setText(response.getJSONObject("data").getJSONObject("post").getString("text"));
                            viewLike.setText(response.getJSONObject("data").getJSONObject("post").getString("like_count") + " like");
                            viewComment.setText(response.getJSONObject("data").getJSONObject("post").getString("comment_count") + " comment");

                            JSONArray comments = response.getJSONObject("data").getJSONArray("comment");
                            for(int i = 0; i < comments.length(); i++){
                                JSONObject c = comments.getJSONObject(i);
                                CommentFJBModel setData = new CommentFJBModel();
                                setData.setnama(c.getJSONObject("user").getString("first_name") + " " + c.getJSONObject("user").getString("last_name"));
                                setData.setcomment(c.getString("text"));
                                setData.setimageprofile(c.getJSONObject("user").getString("photo_profile"));
                                listDataComment.add(setData);
                            }
                            commentfjbadapter.notifyDataSetChanged();
                            scroll.setVisibility(View.VISIBLE);
                        }catch (JSONException e){

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    String komentar = "";
    @OnClick(R.id.btnComment)
    public void btnComment(){
        if(etComment.length() > 2){
            komentar = etComment.getText().toString();
            commentNow();
        }else{
            Toast.makeText(this, "Minimal 3 karakter.", Toast.LENGTH_SHORT).show();
        }
    }

    void commentNow(){
        final MaterialDialog mmDialog = utils.showDialog(this, "Please wait");
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/timeline/comment/" + id)
                .addBodyParameter("token", utils.getDataSession(this, "sessionTOKEN"))
                .addBodyParameter("text", komentar)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mmDialog.dismiss();
                        try{
                            if(response.getString("status").equals("true")){
                                etComment.setText("");
                                CommentFJBModel setData = new CommentFJBModel();
                                setData.setnama(response.getJSONObject("data").getJSONObject("user").getString("first_name") + " " + response.getJSONObject("data").getJSONObject("user").getString("last_name"));
                                setData.setcomment(response.getJSONObject("data").getString("text"));
                                setData.setimageprofile(response.getJSONObject("data").getJSONObject("user").getString("photo_profile"));
                                listDataComment.add(setData);

                                commentfjbadapter.notifyDataSetChanged();
                                loadData();
                            }
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        mmDialog.dismiss();
                    }
                });
    }
}
