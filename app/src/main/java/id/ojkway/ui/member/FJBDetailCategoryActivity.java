package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.FJBCategoryDetailAdapter;
import id.ojkway.model.FJBCategoryDetailModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.HeaderGridView;
import id.ojkway.widgets.MonseratTextView;

public class FJBDetailCategoryActivity extends AppCompatActivity {

    View viewMengatur;
    View viewMengawasi;
    View viewMelindungi;
    View viewUntukIndustri;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    MonseratTextView title;
    @BindView(R.id.viewList)
    HeaderGridView viewList;
    @BindView(R.id.notfound)
    View notfound;

    Bundle data;
    String id;

    FJBCategoryDetailAdapter fjbCategoryDetailAdapter;
    List<FJBCategoryDetailModel> listData = new ArrayList<FJBCategoryDetailModel>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fjbdetailcategory);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();

        data = getIntent().getExtras();

        id = data.getString("id");
        title.setText(data.getString("nama"));
        View header = getLayoutInflater().inflate(R.layout.header_home_fjb, null);
        viewMengatur = header.findViewById(R.id.viewMengatur);
        viewMengawasi = header.findViewById(R.id.viewMengawasi);
        viewMelindungi = header.findViewById(R.id.viewMelindungi);
        viewUntukIndustri = header.findViewById(R.id.viewUntukIndustri);
        viewList.addHeaderView(header);
       /* viewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i != 0){

                }
            }
        });*/

        fjbCategoryDetailAdapter = new FJBCategoryDetailAdapter(this, listData);
        viewList.setAdapter(fjbCategoryDetailAdapter);
        updateView();
        /*ANRequest.PostRequestBuilder load = AndroidNetworking.post("");
        load.addBodyParameter("[]","");*/
    }

    @Override
    protected void onResume(){
        super.onResume();
        loadData();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    void updateView(){
        if(listData.size() > 0){
            notfound.setVisibility(View.GONE);
            viewList.setVisibility(View.VISIBLE);

            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    loadAnimation();
                }
            };
            handler.postDelayed(r, 500);
        }else{
            notfound.setVisibility(View.VISIBLE);
            viewList.setVisibility(View.GONE);
        }
    }

    void loadData(){
        listData.clear();
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/fjb/category/" + id)
                .addBodyParameter("token", utils.getDataSession(this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            JSONArray thread = response.getJSONObject("data").getJSONArray("thread");
                            for(int i = 0 ; i < thread.length() ; i++){
                                JSONObject c = thread.getJSONObject(i);
                                FJBCategoryDetailModel setData = new FJBCategoryDetailModel();
                                setData.setid(c.getString("id"));
                                setData.setnama(c.getString("title"));
                                setData.setharga(c.getString("price"));
                                setData.setimage(c.getString("image"));
                                listData.add(setData);
                            }
                            fjbCategoryDetailAdapter.notifyDataSetChanged();
                            updateView();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        utils.logLargeString("", anError.getErrorBody().toString());
                    }
                });
    }

    void loadAnimation(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewMengatur.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.SlideInLeft)
                                .duration(500)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        viewMengawasi.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        YoYo.with(Techniques.SlideInLeft)
                                                .duration(500)
                                                .withListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {
                                                        viewMelindungi.setVisibility(View.VISIBLE);                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        YoYo.with(Techniques.FadeInUp)
                                                                .duration(500)
                                                                .withListener(new Animator.AnimatorListener() {
                                                                    @Override
                                                                    public void onAnimationStart(Animator animation) {
                                                                        viewUntukIndustri.setVisibility(View.VISIBLE);                                                    }

                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationCancel(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationRepeat(Animator animation) {

                                                                    }
                                                                })
                                                                .playOn(viewUntukIndustri);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                })
                                                .playOn(viewMelindungi);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(viewMengawasi);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewMengatur);
    }

    @OnClick(R.id.btnCreateFJB)
    public void btnCreateFJB(){
        startActivity(new Intent(this, CreateFJBActivity.class));
    }
}
