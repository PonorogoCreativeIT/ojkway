package id.ojkway.ui.member;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.etsy.android.grid.StaggeredGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.GaleryAdapter;
import id.ojkway.adapter.MajalahAdapter;
import id.ojkway.model.GaleryModel;
import id.ojkway.model.MajalahModel;
import id.ojkway.utils.utils;

/**
 * Created by ponorogocreativeit on 21/09/16.
 */

public class MajalahFragment extends Fragment {

    @BindView(R.id.viewMajalah)
    GridView viewMajalah;

    List<MajalahModel> listData = new ArrayList<MajalahModel>();
    MajalahAdapter majalahAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_majalah, container, false);
        ButterKnife.bind(this, view);
        AndroidNetworking.initialize(getActivity());
        majalahAdapter = new MajalahAdapter(getActivity(), listData);
        viewMajalah.setAdapter(majalahAdapter);
        viewMajalah.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final MajalahModel getData = listData.get(i);
                final String pathMajalah= Environment.getExternalStorageDirectory().toString()+"/majalah/";
                final String fileMajalah = getData.geturl().substring(getData.geturl().lastIndexOf('/') + 1);
                File file=new File(pathMajalah + fileMajalah);

                if(file.exists()){
                    Bundle dataIntent = new Bundle();
                    dataIntent.putString("pathImage", pathMajalah + fileMajalah);
                    dataIntent.putString("title", getData.gettext());
                    startActivity(new Intent(getActivity(), DetailMajalahActivity.class).putExtras(dataIntent));
                }else{
                    final MaterialDialog mDialog = utils.showDialog(getActivity(), "Proses download majalah ...");
                    AndroidNetworking.download(ApplicationConstants.SERVER_ASSETS + getData.geturl(), pathMajalah, fileMajalah)
                            .setTag(fileMajalah)
                            .setPriority(Priority.HIGH)
                            .build()
                            .setDownloadProgressListener(new DownloadProgressListener() {
                                @Override
                                public void onProgress(long bytesDownloaded, long totalBytes) {

                                }
                            })
                            .startDownload(new DownloadListener() {
                                @Override
                                public void onDownloadComplete() {
                                    mDialog.dismiss();
                                    Toast.makeText(getActivity(), "Proses download berhasil", Toast.LENGTH_SHORT).show();
                                    Bundle dataIntent = new Bundle();
                                    dataIntent.putString("pathImage", pathMajalah + fileMajalah);
                                    dataIntent.putString("title", getData.gettext());
                                    startActivity(new Intent(getActivity(), DetailMajalahActivity.class).putExtras(dataIntent));
                                }

                                @Override
                                public void onError(ANError anError) {
                                    mDialog.dismiss();
                                    Toast.makeText(getActivity(), "Proses download gagal", Toast.LENGTH_SHORT).show();
                                }
                            });
                }
            }
        });
        /*loadTemp();*/
        loadData();
        return view;
    }

    void loadTemp(){
        for(int i = 0; i < 20; i++){
            MajalahModel setData = new MajalahModel();
            setData.setid("" + i);
            setData.setimage("http://media02.hongkiat.com/ww-flower-wallpapers/blue-rose.jpg");
            setData.settext("ini cuman kata kata aja kok. ciyus gak boong. suer deh.");
            listData.add(setData);
        }
        /*galeryAdapter.notifyDataSetChanged();*/
    }

    void loadData(){
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/majalah?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            JSONArray majalah = response.getJSONObject("data").getJSONArray("majalah");
                            for(int i = 0; i < majalah.length(); i++){
                                JSONObject c = majalah.getJSONObject(i);
                                MajalahModel setData = new MajalahModel();
                                setData.setid(c.getString("id"));
                                setData.setimage(ApplicationConstants.SERVER_ASSETS + c.getString("photo1"));
                                setData.seturl(c.getString("download_link"));
                                setData.settext(c.getString("edisi"));
                                listData.add(setData);
                            }
                            majalahAdapter.notifyDataSetChanged();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }


}
