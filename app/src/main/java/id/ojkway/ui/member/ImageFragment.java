package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.etsy.android.grid.StaggeredGridView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.GaleryAdapter;
import id.ojkway.model.GaleryModel;
import id.ojkway.utils.utils;

/**
 * Created by ponorogocreativeit on 21/09/16.
 */

public class ImageFragment extends Fragment {

    @BindView(R.id.viewGalery)
    GridView viewGalery;

    List<GaleryModel> listData = new ArrayList<GaleryModel>();
    GaleryAdapter galeryAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_image, container, false);
        ButterKnife.bind(this, view);

        galeryAdapter = new GaleryAdapter(getActivity(), listData);
        viewGalery.setAdapter(galeryAdapter);
        viewGalery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GaleryModel getData = listData.get(i);
                Bundle setDataIntent = new Bundle();
                setDataIntent.putString("judul", getData.gettext());
                setDataIntent.putString("url", getData.getimage());
                startActivity(new Intent(getActivity(), ImageActivity.class).putExtras(setDataIntent));
            }
        });
        loadGambar();
        return view;
    }

    int page = 1;
    void loadGambar(){
        Log.v("url", ApplicationConstants.SERVER_API + "api/v1/gallery/image/" + page + "?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"));
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/gallery/image/" + page + "?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.logLargeString(response.toString(), "response");
                        try{
                            JSONArray gallery = response.getJSONObject("data").getJSONArray("gallery");
                            for(int i = 0; i < gallery.length(); i++){
                                JSONObject c = gallery.getJSONObject(i);
                                GaleryModel setData = new GaleryModel();
                                setData.setid(c.getString("id"));
                                setData.setimage(ApplicationConstants.SERVER_ASSETS + "images/albums/" + c.getString("url"));
                                setData.settext(c.getString("title"));
                                listData.add(setData);
                            }

                            galeryAdapter.notifyDataSetChanged();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        utils.logLargeString(anError.getErrorBody(), "error");
                    }
                });

    }


    void loadTemp(){
        for(int i = 0; i < 20; i++){
            GaleryModel setData = new GaleryModel();
            setData.setid("" + i);
            setData.setimage("http://media02.hongkiat.com/ww-flower-wallpapers/blue-rose.jpg");
            setData.settext("ini cuman kata kata aja kok. ciyus gak boong. suer deh.");
            listData.add(setData);
        }

        galeryAdapter.notifyDataSetChanged();
    }


}
