package id.ojkway.ui.member;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.FJBCategoryDetailAdapter;
import id.ojkway.adapter.FileSharingDetailAdapter;
import id.ojkway.model.DetailFileSharingModel;
import id.ojkway.model.FJBCategoryDetailModel;
import id.ojkway.model.FileSharingModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.HeaderGridView;
import id.ojkway.widgets.MonseratTextView;

public class DetailFileSharingActivity extends AppCompatActivity {

    View viewMengatur;
    View viewMengawasi;
    View viewMelindungi;
    View viewUntukIndustri;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    MonseratTextView title;
    @BindView(R.id.viewList)
    ListView viewList;
    @BindView(R.id.notfound)
    View notfound;

    Bundle data;
    String id;

    private long enqueue;
    private DownloadManager dm;

    FileSharingDetailAdapter fileSharingDetailAdapter;
    List<DetailFileSharingModel> listData = new ArrayList<DetailFileSharingModel>();
    BroadcastReceiver receiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailfilesharing);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();

        data = getIntent().getExtras();

        id = data.getString("id");
        title.setText(data.getString("nama"));
        icon = data.getString("icon");
        View header = getLayoutInflater().inflate(R.layout.header_home_fjb, null);
        viewMengatur = header.findViewById(R.id.viewMengatur);
        viewMengawasi = header.findViewById(R.id.viewMengawasi);
        viewMelindungi = header.findViewById(R.id.viewMelindungi);
        viewUntukIndustri = header.findViewById(R.id.viewUntukIndustri);
        viewList.addHeaderView(header);
        viewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DetailFileSharingModel getData = listData.get(i - 1);
                Log.v("url : ", ApplicationConstants.SERVER_ASSETS + "images/file_sharing/" + getData.geturl());
                dm = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(
                        Uri.parse(ApplicationConstants.SERVER_ASSETS + "images/file_sharing/" + getData.geturl().replace(" ","%20")));
                enqueue = dm.enqueue(request);
                Toast.makeText(DetailFileSharingActivity.this, "Proses download sudah dimulai", Toast.LENGTH_SHORT).show();
                registerReceiver(receiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
            }
        });

        fileSharingDetailAdapter = new FileSharingDetailAdapter(this, listData);
        viewList.setAdapter(fileSharingDetailAdapter);
        updateView();
        /*ANRequest.PostRequestBuilder load = AndroidNetworking.post("");
        load.addBodyParameter("[]","");*/

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(
                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    DownloadManager.Query query = new DownloadManager.Query();
                    query.setFilterById(enqueue);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            //Toast.makeText(DetailFileSharingActivity.this, "detail : " + c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI)), Toast.LENGTH_SHORT).show();
                            Toast.makeText(DetailFileSharingActivity.this, "File berhasil didownload", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent();
                            i.setAction(DownloadManager.ACTION_VIEW_DOWNLOADS);
                            startActivity(i);
                        }
                    }
                }
            }
        };


    }

    @Override
    protected void onResume(){
        super.onResume();
        loadData();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    void updateView(){
        if(listData.size() > 0){
            notfound.setVisibility(View.GONE);
            viewList.setVisibility(View.VISIBLE);

            Handler handler = new Handler();
            Runnable r = new Runnable() {
                public void run() {
                    loadAnimation();
                }
            };
            handler.postDelayed(r, 500);
        }else{
            notfound.setVisibility(View.VISIBLE);
            viewList.setVisibility(View.GONE);
        }
    }

    String icon;

    void loadData(){
        listData.clear();
        Log.v("url", ApplicationConstants.SERVER_API + "api/v1/filesharing/category/" + id + "?token=" + utils.getDataSession(this, "sessionTOKEN"));
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/filesharing/category/" + id + "?token=" + utils.getDataSession(this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.v("response", response.toString());
                        try{
                            JSONArray files = response.getJSONObject("data").getJSONArray("files");
                            for(int i = 0 ; i < files.length() ; i++){
                                JSONObject c = files.getJSONObject(i);
                                DetailFileSharingModel setData = new DetailFileSharingModel();
                                setData.setid(c.getString("id"));
                                setData.setnama(c.getString("title"));
                                setData.setimage(c.getJSONObject("uploader").getString("profile_photo"));
                                setData.setdeskripsi(c.getString("description"));
                                setData.seturl(c.getString("file_path"));
                                listData.add(setData);
                            }
                            fileSharingDetailAdapter.notifyDataSetChanged();
                            updateView();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        utils.logLargeString("", anError.getErrorBody().toString());
                    }
                });
    }

    void loadAnimation(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewMengatur.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.SlideInLeft)
                                .duration(500)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        viewMengawasi.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        YoYo.with(Techniques.SlideInLeft)
                                                .duration(500)
                                                .withListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {
                                                        viewMelindungi.setVisibility(View.VISIBLE);                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        YoYo.with(Techniques.FadeInUp)
                                                                .duration(500)
                                                                .withListener(new Animator.AnimatorListener() {
                                                                    @Override
                                                                    public void onAnimationStart(Animator animation) {
                                                                        viewUntukIndustri.setVisibility(View.VISIBLE);                                                    }

                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationCancel(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationRepeat(Animator animation) {

                                                                    }
                                                                })
                                                                .playOn(viewUntukIndustri);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                })
                                                .playOn(viewMelindungi);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(viewMengawasi);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewMengatur);
    }

}
