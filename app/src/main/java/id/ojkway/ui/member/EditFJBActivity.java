package id.ojkway.ui.member;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.AddImageAdapter;
import id.ojkway.model.AddImageModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;
import id.ojkway.widgets.MonseratTextView;
import id.ojkway.widgets.WrapContentGridView;

public class EditFJBActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    MonseratTextView title;
    @BindView(R.id.tempImage)
    ImageView tempImage;
    @BindView(R.id.dataImage)
    WrapContentGridView dataImage;
    @BindView(R.id.category)
    MonseratEditText category;
    @BindView(R.id.judul)
    MonseratEditText judul;
    @BindView(R.id.harga)
    MonseratEditText harga;
    @BindView(R.id.deskripsi)
    MonseratEditText deskripsi;

    AddImageAdapter addImageAdapter;
    List<AddImageModel> listDataImage = new ArrayList<>();

    String id;
    Bundle data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_fjb);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();

        data = getIntent().getExtras();
        id = data.getString("id");
        utils.setEditTextAsCurrency(harga);
        category.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(MotionEvent.ACTION_UP == motionEvent.getAction()) {
                    callCategory();
                }
                return true;
            }
        });

        addImageAdapter = new AddImageAdapter(this, listDataImage);
        dataImage.setAdapter(addImageAdapter);

        loadKategory();
        loadData();
    }

    void loadData(){
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/fjb/edit_thread/" + id + "?token=" + utils.getDataSession(this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            selectedCategory = response.getJSONObject("data").getJSONObject("thread").getString("category_id");
                            category.setText(response.getJSONObject("data").getJSONObject("thread").getString("category_title"));
                            judul.setText(response.getJSONObject("data").getJSONObject("thread").getString("title"));
                            deskripsi.setText(response.getJSONObject("data").getJSONObject("thread").getString("description"));
                            harga.setText(utils.setCurrencyWithOutRp(response.getJSONObject("data").getJSONObject("thread").getString("price")));

                            JSONArray images = response.getJSONObject("data").getJSONObject("thread").getJSONArray("images");
                            for(int i = 0; i < images.length(); i++){
                                JSONObject c = images.getJSONObject(i);
                                AddImageModel setData = new AddImageModel();
                                setData.setimage(c.getString("filename"));
                                setData.settype("url");
                                listDataImage.add(setData);
                            }
                            addImageAdapter.notifyDataSetChanged();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    List<String> listNamaCategory = new ArrayList<>();
    List<String> listIDCategory = new ArrayList<>();

    void loadKategory(){
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/fjb")
                .addBodyParameter("token", utils.getDataSession(EditFJBActivity.this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if(response.getString("status").equals("true")){
                                JSONArray categories = response.getJSONObject("data").getJSONArray("categories");

                                for(int i = 0 ; i < categories.length(); i++){
                                    JSONObject c = categories.getJSONObject(i);
                                    listNamaCategory.add(c.getString("category_title"));
                                    listIDCategory.add(c.getString("category_id"));
                                }
                            }else if(response.getString("status").equals("false")){
                                Toast.makeText(EditFJBActivity.this, response.getString("pesan"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
    String selectedCategory = "";
    void callCategory(){
        final CharSequence[] options = listNamaCategory.toArray(new String[0]);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                category.setText(listNamaCategory.get(which));
                selectedCategory = listIDCategory.get(which);
            }
        });
        builder.show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public static String getDataImage(ImageView img){
        String data = "";
        img.buildDrawingCache();
        BitmapDrawable drawable = (BitmapDrawable) img.getDrawable();
        Bitmap bitmap = drawable.getBitmap();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bb = bos.toByteArray();
        data = Base64.encodeToString(bb, 0);
        return data;
    }

    private void openCameraForResult(int requestCode){
        Intent photo = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Uri uri  = Uri.parse("file:///sdcard/photo_" + requestCode + ".jpg");
        photo.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        startActivityForResult(photo, requestCode);
    }

    private void openGaleryForResult(int requestCode){
        Intent intent = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                requestCode);
    }

    private void callDialogCameraAndGalery(final int requestCodeCamera, final int requestCodeGalery){
        final CharSequence[] options = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                if (options[which].equals("Take Photo")) {
                    openCameraForResult(requestCodeCamera);
                } else if (options[which].equals("Choose from Gallery")) {
                    openGaleryForResult(requestCodeGalery);
                } else if (options[which].equals("Cancel")) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }

    @OnClick(R.id.btnAddGambar)
    public void btnAddGambar(){
        callDialogCameraAndGalery(0, 1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 0) {
                onCaptureFromCameraResult(tempImage, requestCode);
            }else if (requestCode == 1) {
                onSelectFromGalleryResult(tempImage, data);
            }
        }
    }

    private void onSelectFromGalleryResult(ImageView view, Intent data) {
        try{
            view.setImageBitmap(decodeStreamData(data.getData()));
            AddImageModel setData = new AddImageModel();
            setData.setimage(getDataImage(tempImage));
            setData.settype("string");
            listDataImage.add(setData);
            addImageAdapter.notifyDataSetChanged();
        }catch (FileNotFoundException e){

        }
    }

    private void onCaptureFromCameraResult(ImageView view, int requestCode){
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "photo_" + requestCode +".jpg");
        Uri uri = Uri.fromFile(file);
        Bitmap bitmap;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
            view.setImageBitmap(decodeStreamData(uri));
            AddImageModel setData = new AddImageModel();
            setData.setimage(getDataImage(tempImage));
            setData.settype("string");
            listDataImage.add(setData);
            addImageAdapter.notifyDataSetChanged();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private Bitmap decodeStreamData(Uri selectedImage) throws FileNotFoundException {

        // Decode image size
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 150;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        int scale = 1;
        while (true) {
            if (width_tmp / 2 < REQUIRED_SIZE
                    || height_tmp / 2 < REQUIRED_SIZE) {
                break;
            }
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        return BitmapFactory.decodeStream(getContentResolver().openInputStream(selectedImage), null, o2);

    }

    List<String> imageUpload = new ArrayList<>();
    @OnClick(R.id.btnUpload)
    public void btnUpload(){
        int error = 0;
        if(selectedCategory.equals("")){
            error++;
            Toast.makeText(this, "Field kategori tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        }
        if(judul.getText().toString().equals("")){
            error++;
            Toast.makeText(this, "Field judul tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        }
        if(harga.getText().toString().equals("")){
            error++;
            Toast.makeText(this, "Field harga tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        }
        if(deskripsi.getText().toString().equals("")){
            error++;
            Toast.makeText(this, "Field deskripsi tidak boleh kosong.", Toast.LENGTH_SHORT).show();
        }
        if(dataImage.getAdapter().getCount() == 0){
            error++;
            Toast.makeText(this, "Anda belum memilih gambar untuk thread ini.", Toast.LENGTH_SHORT).show();
        }

        if(error == 0){
            for (int i = 0; i < dataImage.getAdapter().getCount(); i++) {
                View view = dataImage.getAdapter().getView(i, null, null);
                ImageView imageFJB = (ImageView) view.findViewById(R.id.imageFJB);
                imageUpload.add(getDataImage(imageFJB));
            }
            uploadThread();
        }
        //Toast.makeText(this, "total : " + imageUpload.size(), Toast.LENGTH_SHORT).show();
    }

    void uploadThread(){
        final MaterialDialog mDIalog = utils.showDialog(this, "Please wait");
        ANRequest.PostRequestBuilder PostData = AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/fjb/update_thread_android/" + id);
        PostData.addBodyParameter("category", selectedCategory);
        for(int i = 0; i < imageUpload.size(); i++){
            PostData.addBodyParameter("photo[" + i + "]",imageUpload.get(i));
        }
        PostData.addBodyParameter("title", judul.getText().toString());
        PostData.addBodyParameter("description", deskripsi.getText().toString());
        PostData.addBodyParameter("price", harga.getText().toString().replace(".",""));
        PostData.addBodyParameter("token", utils.getDataSession(this, "sessionTOKEN"));
        PostData.build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                mDIalog.dismiss();
                try{
                    if(response.getString("status").equals("true")){
                        Toast.makeText(EditFJBActivity.this, "Thread berhasil diedit!", Toast.LENGTH_SHORT).show();
                        finish();
                    }else{
                        Toast.makeText(EditFJBActivity.this, "Thread gagal diedit!", Toast.LENGTH_SHORT).show();
                    }
                }catch (JSONException e){

                }
            }

            @Override
            public void onError(ANError anError) {
                mDIalog.dismiss();
                //utils.logLargeString("errornya", anError.getErrorBody());
                Toast.makeText(EditFJBActivity.this, "Thread gagal diedit", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
