package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.FJBCategoryAdapter;
import id.ojkway.model.FJBCategoryModel;
import id.ojkway.utils.utils;

/**
 * Created by ponorogocreativeit on 21/09/16.
 */

public class FJBCategoryFragment extends Fragment {

    View viewMengatur;
    View viewMengawasi;
    View viewMelindungi;
    View viewUntukIndustri;


    @BindView(R.id.viewList)
    ListView viewList;

    List<FJBCategoryModel> listFJBCategory = new ArrayList<>();
    FJBCategoryAdapter fjbCategoryAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fjbcategory, container, false);
        ButterKnife.bind(this, view);

        AndroidNetworking.initialize(getActivity());
        fjbCategoryAdapter = new FJBCategoryAdapter(getActivity(), listFJBCategory);
        viewList.setAdapter(fjbCategoryAdapter);
        viewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            if(i  != 0){
                FJBCategoryModel getData = listFJBCategory.get(i - 1);
                // Toast.makeText(getActivity(), "getData.getid() : " + getData.getid(), Toast.LENGTH_SHORT).show();
                Bundle data = new Bundle();
                data.putString("id", getData.getid());
                data.putString("nama", getData.getnama());
                startActivity(new Intent(getActivity(), FJBDetailCategoryActivity.class).putExtras(data));
            }
            }
        });
        //viewList.setTransitionEffect(new CardsEffect());

        View header = inflater.inflate(R.layout.header_home_fjb, null);
        viewMengatur = header.findViewById(R.id.viewMengatur);
        viewMengawasi = header.findViewById(R.id.viewMengawasi);
        viewMelindungi = header.findViewById(R.id.viewMelindungi);
        viewUntukIndustri = header.findViewById(R.id.viewUntukIndustri);



        viewList.addHeaderView(header);

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                loadAnimation();
            }
        };
        handler.postDelayed(r, 500);

        Handler handlerea = new Handler();
        Runnable re = new Runnable() {
            public void run() {
                loadData();
                //loadDataTemp();
            }
        };
        handlerea.postDelayed(re, 100);



        return view;

    }


    void loadDataTemp(){
        String icon[] = {"fa-anchor", "fa-angle-left", "fa-apple", "fa-android", "fa-bed", "fa-cc", "fa-cny", "fa-dribbble", "fa-glass", "fa-hourglass-o"};
        for(int i = 0; i < 10; i++) {
            FJBCategoryModel setData = new FJBCategoryModel();
            setData.setid("" + i);
            setData.setnama("Nama " + i);
            setData.setdeskripsi(i + " Ini Sebenarnya Deskripsi, beneran lho");
            setData.seticon(icon[i]);
            listFJBCategory.add(setData);
        }
        fjbCategoryAdapter.notifyDataSetChanged();
        //Toast.makeText(getActivity(), "listDashboard.size();" + listDashboard.size(), Toast.LENGTH_SHORT).show();

    }
    void loadAnimation(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewMengatur.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.SlideInLeft)
                                .duration(500)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        viewMengawasi.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        YoYo.with(Techniques.SlideInLeft)
                                                .duration(500)
                                                .withListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {
                                                        viewMelindungi.setVisibility(View.VISIBLE);                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        YoYo.with(Techniques.FadeInUp)
                                                                .duration(500)
                                                                .withListener(new Animator.AnimatorListener() {
                                                                    @Override
                                                                    public void onAnimationStart(Animator animation) {
                                                                        viewUntukIndustri.setVisibility(View.VISIBLE);                                                    }

                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationCancel(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationRepeat(Animator animation) {

                                                                    }
                                                                })
                                                                .playOn(viewUntukIndustri);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                })
                                                .playOn(viewMelindungi);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(viewMengawasi);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewMengatur);
    }

    void loadData(){
        Log.v("sessionTOKEN", utils.getDataSession(getActivity(), "sessionTOKEN"));
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/fjb")
                .addBodyParameter("token", utils.getDataSession(getActivity(), "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if(response.getString("status").equals("true")){
                                JSONArray categories = response.getJSONObject("data").getJSONArray("categories");

                                for(int i = 0 ; i < categories.length(); i++){
                                    JSONObject c = categories.getJSONObject(i);
                                    FJBCategoryModel setData = new FJBCategoryModel();
                                    setData.setid(c.getString("category_id"));
                                    setData.setnama(c.getString("category_title"));
                                    setData.setdeskripsi(c.getString("category_description"));
                                    setData.seticon(c.getString("category_icon").replace("fa ", "").replace(" ", ""));
                                    listFJBCategory.add(setData);
                                }
                                fjbCategoryAdapter.notifyDataSetChanged();
                            }else if(response.getString("status").equals("false")){
                                Toast.makeText(getActivity(), response.getString("pesan"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){

                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    @OnClick(R.id.btnCreateFJB)
    public void btnCreateFJB(){
        startActivity(new Intent(getActivity(), CreateFJBActivity.class));
    }

}
