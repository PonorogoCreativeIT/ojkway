package id.ojkway.ui.member;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.ChatAdapter;
import id.ojkway.model.ChatModel;
import id.ojkway.model.ChatModelNew;
import id.ojkway.model.MessageModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;
import id.ojkway.widgets.MonseratTextView;

public class ChatActivity extends AppCompatActivity {

    @BindView(R.id.viewList)
    ListView viewList;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.title)
    MonseratTextView title;
    @BindView(R.id.viewTextChat)
    MonseratEditText viewTextChat;

    List<ChatModel> listData = new ArrayList<ChatModel>();
    ChatAdapter chatAdapter;

    Bundle data;
    String id;
    String idSaya;

    FirebaseDatabase database;
    DatabaseReference refDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();
        data = getIntent().getExtras();
        id = data.getString("id");
        title.setText(data.getString("title"));
        chatAdapter = new ChatAdapter(this, listData);
        viewList.setAdapter(chatAdapter);
        idSaya = utils.getDataSession(this, "sessionID");
        String currentChat = String.valueOf((Integer.valueOf(idSaya) * Integer.valueOf(idSaya)) + (Integer.valueOf(id) * Integer.valueOf(id)));
        database = FirebaseDatabase.getInstance();
        refDB = database.getReference("message_" + currentChat);
        Log.v("idSaya", idSaya);
        Log.v("id", id);
        Log.v("message_" , "message_" + currentChat);
        refDB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listData.clear();
                for (DataSnapshot child : dataSnapshot.getChildren()) {
                    ChatModelNew post = child.getValue(ChatModelNew.class);
                    Log.v("post.text : ", post.text );
                    ChatModel setData = new ChatModel();
                    setData.setid(post.id);
                    setData.setprofile(ApplicationConstants.SERVER_ASSETS + post.profile_picture);
                    setData.setmessage(post.text);
                    listData.add(setData);
                }
                chatAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    void loadData(){
        for(int i = 0 ; i < 15; i++){
            ChatModel setData = new ChatModel();
            setData.setid("" + i);
            setData.setprofile("https://organicthemes.com/demo/profile/files/2012/12/profile_img.png");
            setData.setmessage("ini sih random chat, seriusan gak boong saya.");
            listData.add(setData);
        }
        chatAdapter.notifyDataSetChanged();
        //scrollMyListViewToBottom();
    }

    String text = "";
    @OnClick(R.id.btnKirim)
    public void btnKirim(){
        if(viewTextChat.getText().length() > 0){
            ChatModelNew chatData = new ChatModelNew(viewTextChat.getText().toString(), utils.getDataSession(this, "sessionEMAIL"), utils.getTimeNow(), utils.getDataSession(this, "sessionPHOTOPROFILE"), utils.getDataSession(this, "sessionUSERNAME"), utils.getDataSession(this, "sessionID"));
            refDB.push().setValue(chatData);/*
            ChatModel setData = new ChatModel();
            setData.setid("2");
            setData.setprofile("https://organicthemes.com/demo/profile/files/2012/12/profile_img.png");
            setData.setmessage(viewTextChat.getText().toString());
            listData.add(setData);*/
            text = viewTextChat.getText().toString();
            kirimLatestChat();
            viewTextChat.setText("");
        }
        //chatAdapter.notifyDataSetChanged();
        //scrollMyListViewToBottom();
    }

    private void scrollMyListViewToBottom() {
        viewList.post(new Runnable() {
            @Override
            public void run() {
                // Select the last row so it will scroll into view...
                viewList.setSelection(chatAdapter.getCount() - 1);
            }
        });
    }


    void kirimLatestChat(){
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/latest_chat")
                .addBodyParameter("token", utils.getDataSession(this, "sessionTOKEN"))
                .addBodyParameter("user_id_from", idSaya)
                .addBodyParameter("user_id_to", id)
                .addBodyParameter("text", text)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            if(response.getString("status").equals("true")){
                                //Toast.makeText(ChatActivity.this, "sukses", Toast.LENGTH_SHORT).show();
                            }else{
                                //Toast.makeText(ChatActivity.this, "gagal", Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        //Toast.makeText(ChatActivity.this, "gagal!", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
