package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.MessageAdapter;
import id.ojkway.model.MessageModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;

public class MessageActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.viewList)
    ListView viewList;

    List<MessageModel> listData = new ArrayList<MessageModel>();
    MessageAdapter messageAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.bringToFront();
        messageAdapter = new MessageAdapter(this, listData);
        viewList.setAdapter(messageAdapter);
        viewList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                MessageModel getData = listData.get(i);
                Bundle setDataIntent = new Bundle();
                setDataIntent.putString("id", getData.getid());
                setDataIntent.putString("title", getData.getnama());
                startActivity(new Intent(MessageActivity.this, ChatActivity.class).putExtras(setDataIntent));
            }
        });


    }

    @Override
    protected void onResume(){
        super.onResume();
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    ArrayList<String> sudahAda = new ArrayList<String>();

    void loadData(){
        listData.clear();
        messageAdapter.notifyDataSetChanged();
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/latest_chat?token=" + utils.getDataSession(this, "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        sudahAda.clear();
                        Log.v("response", response.toString());
                        try{

                            JSONArray chat = response.getJSONObject("data").getJSONArray("chat");
                            boolean isFirst = true;
                            for(int i = 0; i < chat.length(); i++){
                                String id = "";
                                JSONObject c = chat.getJSONObject(i);
                                MessageModel setData = new MessageModel();
                                if(c.getJSONObject("from").getString("id").equals(utils.getDataSession(MessageActivity.this, "sessionID"))){
                                    id = c.getJSONObject("to").getString("id");
                                    setData.setid(c.getJSONObject("to").getString("id"));
                                    setData.setnama(c.getJSONObject("to").getString("first_name") + " " + c.getJSONObject("to").getString("last_name"));
                                    setData.setphoto(ApplicationConstants.SERVER_ASSETS + c.getJSONObject("to").getString("photo_profile"));
                                }else{
                                    id = c.getJSONObject("from").getString("id");
                                    setData.setid(c.getJSONObject("from").getString("id"));
                                    setData.setnama(c.getJSONObject("from").getString("first_name") + " " + c.getJSONObject("from").getString("last_name"));
                                    setData.setphoto(ApplicationConstants.SERVER_ASSETS + c.getJSONObject("from").getString("photo_profile"));
                                }
                                setData.setmessage(c.getString("text"));
                                setData.settanggal(c.getJSONObject("timestamp").getString("date"));

                                if(!sudahAda.contains(id)){
                                    sudahAda.add(id);
                                    listData.add(setData);
                                }

                            }
                            listData.toString();
                            messageAdapter.notifyDataSetChanged();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
    void tempData(){
        for(int i = 0; i < 10; i++){
            MessageModel setData = new MessageModel();
            setData.setid("" + i);
            setData.setnama("Hamba Allah " + i);
            setData.setmessage("Ini sebenarnya pesan rahasia sih, tapi udah gak jadi rahasia lagi sih");
            setData.settanggal("1 Menit yang lalu");
            setData.setphoto("https://organicthemes.com/demo/profile/files/2012/12/profile_img.png");
            listData.add(setData);
        }
        messageAdapter.notifyDataSetChanged();
    }

}
