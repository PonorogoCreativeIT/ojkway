package id.ojkway.ui.member;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.twotoasters.jazzylistview.JazzyEffect;
import com.twotoasters.jazzylistview.JazzyListView;
import com.twotoasters.jazzylistview.effects.CardsEffect;
import com.twotoasters.jazzylistview.effects.WaveEffect;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.DashboardAdapter;
import id.ojkway.adapter.DashboardAdapterr;
import id.ojkway.model.DashboardModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.WrapContentListView;

/**
 * Created by ponorogocreativeit on 21/09/16.
 */

public class HomeFragment extends Fragment {

    View viewMengatur;
    View viewMengawasi;
    View viewMelindungi;
    View viewUntukIndustri;
    //button
    View btnBerita;
    View btnAgenda;
    View btnGalery;
    View btnForum;

    View viewBerita;
    View viewAgenda;
    View viewGalery;
    View viewForum;


    @BindView(R.id.viewList)
    ListView viewList;

    List<DashboardModel> listDashboard = new ArrayList<>();
    DashboardAdapter dashboardAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        dashboardAdapter = new DashboardAdapter(getActivity(), listDashboard);
        viewList.setAdapter(dashboardAdapter);
        //viewList.setTransitionEffect(new CardsEffect());

        View header = inflater.inflate(R.layout.header_home, null);
        viewMengatur = header.findViewById(R.id.viewMengatur);
        viewMengawasi = header.findViewById(R.id.viewMengawasi);
        viewMelindungi = header.findViewById(R.id.viewMelindungi);
        viewUntukIndustri = header.findViewById(R.id.viewUntukIndustri);

        btnBerita = header.findViewById(R.id.btnBerita);
        btnAgenda = header.findViewById(R.id.btnAgenda);
        btnGalery = header.findViewById(R.id.btnGalery);
        btnForum = header.findViewById(R.id.btnForum);

        viewBerita = header.findViewById(R.id.viewBerita);
        viewAgenda = header.findViewById(R.id.viewAgenda);
        viewGalery = header.findViewById(R.id.viewGalery);
        viewForum = header.findViewById(R.id.viewForum);

        viewList.addHeaderView(header);

        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                loadAnimation();
            }
        };
        handler.postDelayed(r, 500);

        Handler handlerea = new Handler();
        Runnable re = new Runnable() {
            public void run() {
                loadData();
            }
        };
        handlerea.postDelayed(re, 500);

        btnBerita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewBerita.getVisibility() == View.VISIBLE){
                    closeBerita();
                }else{
                    openBerita();
                }
            }
        });

        btnAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewAgenda.getVisibility() == View.VISIBLE) {
                    closeAgenda();
                }else{
                    openAgenda();
                }
            }
        });

        btnGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewGalery.getVisibility() == View.VISIBLE) {
                    closeGalery();
                }else {
                    openGalery();
                }
            }
        });

        btnForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(viewForum.getVisibility() == View.VISIBLE) {
                    closeForum();
                }else {
                    openForum();
                }
            }
        });

        return view;

    }

    void openBerita(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewBerita.setVisibility(View.VISIBLE);                                                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewBerita);

        if(viewAgenda.getVisibility() == View.VISIBLE){
            closeAgenda();
        }
        if(viewGalery.getVisibility() == View.VISIBLE){
            closeGalery();
        }
        if(viewForum.getVisibility() == View.VISIBLE){
            closeForum();
        }
    }

    void closeBerita(){
        YoYo.with(Techniques.SlideOutRight)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        viewBerita.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewBerita);
    }

    void openAgenda(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewAgenda.setVisibility(View.VISIBLE);                                                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewAgenda);

        if(viewBerita.getVisibility() == View.VISIBLE){
            closeBerita();
        }
        if(viewGalery.getVisibility() == View.VISIBLE){
            closeGalery();
        }
        if(viewForum.getVisibility() == View.VISIBLE){
            closeForum();
        }
    }

    void closeAgenda(){
        YoYo.with(Techniques.SlideOutRight)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        viewAgenda.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewAgenda);
    }

    void openGalery(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewGalery.setVisibility(View.VISIBLE);                                                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewGalery);

        if(viewBerita.getVisibility() == View.VISIBLE){
            closeBerita();
        }
        if(viewAgenda.getVisibility() == View.VISIBLE){
            closeAgenda();
        }
        if(viewForum.getVisibility() == View.VISIBLE){
            closeForum();
        }
    }

    void closeGalery(){
        YoYo.with(Techniques.SlideOutRight)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        viewGalery.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewGalery);
    }

    void openForum(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewForum.setVisibility(View.VISIBLE);                                                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewForum);

        if(viewBerita.getVisibility() == View.VISIBLE){
            closeBerita();
        }
        if(viewAgenda.getVisibility() == View.VISIBLE){
            closeAgenda();
        }
        if(viewGalery.getVisibility() == View.VISIBLE){
            closeGalery();
        }
    }

    void closeForum(){
        YoYo.with(Techniques.SlideOutRight)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        viewForum.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewForum);
    }


    void loadData(){
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/timeline?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try{
                            JSONArray post = response.getJSONObject("data").getJSONArray("post");
                            for(int i = 0 ; i < post.length(); i++){
                                JSONObject c = post.getJSONObject(i);
                                DashboardModel setData = new DashboardModel();
                                setData.setid(c.getString("id"));
                                setData.setnama(c.getJSONObject("user").getString("first_name") + " " + c.getJSONObject("user").getString("last_name"));
                                setData.settime(utils.getTimeAgo(c.getString("timestamp")));
                                setData.setimageprofile(c.getJSONObject("user").getString("photo_profile"));
                                setData.setimage(c.getString("image"));
                                setData.setdeskripsi(c.getString("text"));
                                setData.settotalComment(c.getString("comment_count"));
                                setData.settotalLike(c.getString("like_count"));
                                setData.setliked(c.getString("is_liked"));
                                listDashboard.add(setData);
                            }
                            dashboardAdapter.notifyDataSetChanged();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    void loadDataTemp(){
        for(int i = 0; i < 10; i++) {
            DashboardModel setData = new DashboardModel();
            setData.setid("" + i);
            setData.setnama("Hamba Allah " + i);
            setData.settime(i + " menit");
            setData.setimageprofile("https://organicthemes.com/demo/profile/files/2012/12/profile_img.png");
            setData.setimage("http://media02.hongkiat.com/ww-flower-wallpapers/blue-rose.jpg");
            setData.setdeskripsi("Anggap saja ini adalah deskripsi, di mana kami tidak tahu harus mengisi apa lagi untuk section ini. jadi mohon maaf ya.");
            setData.settotalComment("10" + i);
            setData.settotalLike("10" + i);
            listDashboard.add(setData);
        }
        dashboardAdapter.notifyDataSetChanged();
        //Toast.makeText(getActivity(), "listDashboard.size();" + listDashboard.size(), Toast.LENGTH_SHORT).show();

    }
    void loadAnimation(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewMengatur.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.SlideInLeft)
                                .duration(500)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        viewMengawasi.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        YoYo.with(Techniques.SlideInLeft)
                                                .duration(500)
                                                .withListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {
                                                        viewMelindungi.setVisibility(View.VISIBLE);                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        YoYo.with(Techniques.FadeInUp)
                                                                .duration(500)
                                                                .withListener(new Animator.AnimatorListener() {
                                                                    @Override
                                                                    public void onAnimationStart(Animator animation) {
                                                                        viewUntukIndustri.setVisibility(View.VISIBLE);                                                    }

                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationCancel(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationRepeat(Animator animation) {

                                                                    }
                                                                })
                                                                .playOn(viewUntukIndustri);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                })
                                                .playOn(viewMelindungi);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(viewMengawasi);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewMengatur);
    }

}
