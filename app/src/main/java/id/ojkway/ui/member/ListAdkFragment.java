package id.ojkway.ui.member;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.adapter.ADKAdapter;
import id.ojkway.adapter.GaleryAdapter;
import id.ojkway.model.ADKModel;
import id.ojkway.model.GaleryModel;
import id.ojkway.utils.utils;

/**
 * Created by ponorogocreativeit on 21/09/16.
 */

public class ListAdkFragment extends Fragment {

    View viewMengatur;
    View viewMengawasi;
    View viewMelindungi;
    View viewUntukIndustri;

    @BindView(R.id.viewADk)
    ListView viewADk;

    Button btnTiketSaya;

    List<ADKModel> listData = new ArrayList<ADKModel>();
    ADKAdapter adkAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listadk, container, false);
        ButterKnife.bind(this, view);

        adkAdapter = new ADKAdapter(getActivity(), listData);
        viewADk.setAdapter(adkAdapter);
        viewADk.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ADKModel getData = listData.get(i);
                Bundle setDataIntent = new Bundle();
                setDataIntent.putString("id", getData.getid());
                setDataIntent.putString("nama", getData.getnama());
                startActivity(new Intent(getActivity(), DetailADKActivity.class).putExtras(setDataIntent));
            }
        });

        View header = inflater.inflate(R.layout.header_adk, null);
        viewMengatur = header.findViewById(R.id.viewMengatur);
        viewMengawasi = header.findViewById(R.id.viewMengawasi);
        viewMelindungi = header.findViewById(R.id.viewMelindungi);
        viewUntukIndustri = header.findViewById(R.id.viewUntukIndustri);
        btnTiketSaya = (Button) header.findViewById(R.id.btnTiketSaya);

        btnTiketSaya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TiketSayaADKActivity.class));
            }
        });

        viewADk.addHeaderView(header);
        Handler handler = new Handler();
        Runnable r = new Runnable() {
            public void run() {
                loadAnimation();
            }
        };
        handler.postDelayed(r, 500);
        loadData();
        return view;
    }

    int page = 1;
    void loadData(){
        //Log.v("url", ApplicationConstants.SERVER_API + "api/v1/gallery/image/" + page + "?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"));
        AndroidNetworking.get(ApplicationConstants.SERVER_API + "api/v1/adk?token=" + utils.getDataSession(getActivity(), "sessionTOKEN"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        utils.logLargeString(response.toString(), "response");
                        try{
                            JSONArray adk = response.getJSONObject("data").getJSONArray("adk");
                            for(int i = 0; i < adk.length(); i++){
                                JSONObject c = adk.getJSONObject(i);
                                ADKModel setData = new ADKModel();
                                setData.setid(c.getString("id"));
                                setData.setimage(c.getString("photo_profile"));
                                setData.setdeskripsi("deskripsi");
                                setData.setnama(c.getString("first_name") + " " + c.getString("last_name"));
                                listData.add(setData);
                            }

                            adkAdapter.notifyDataSetChanged();
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        utils.logLargeString(anError.getErrorBody(), "error");
                    }
                });

    }

    void loadAnimation(){
        YoYo.with(Techniques.SlideInLeft)
                .duration(500)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        viewMengatur.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        YoYo.with(Techniques.SlideInLeft)
                                .duration(500)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {
                                        viewMengawasi.setVisibility(View.VISIBLE);
                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        YoYo.with(Techniques.SlideInLeft)
                                                .duration(500)
                                                .withListener(new Animator.AnimatorListener() {
                                                    @Override
                                                    public void onAnimationStart(Animator animation) {
                                                        viewMelindungi.setVisibility(View.VISIBLE);                                                    }

                                                    @Override
                                                    public void onAnimationEnd(Animator animation) {
                                                        YoYo.with(Techniques.FadeInUp)
                                                                .duration(500)
                                                                .withListener(new Animator.AnimatorListener() {
                                                                    @Override
                                                                    public void onAnimationStart(Animator animation) {
                                                                        viewUntukIndustri.setVisibility(View.VISIBLE);                                                    }

                                                                    @Override
                                                                    public void onAnimationEnd(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationCancel(Animator animation) {

                                                                    }

                                                                    @Override
                                                                    public void onAnimationRepeat(Animator animation) {

                                                                    }
                                                                })
                                                                .playOn(viewUntukIndustri);
                                                    }

                                                    @Override
                                                    public void onAnimationCancel(Animator animation) {

                                                    }

                                                    @Override
                                                    public void onAnimationRepeat(Animator animation) {

                                                    }
                                                })
                                                .playOn(viewMelindungi);
                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(viewMengawasi);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(viewMengatur);
    }

}
