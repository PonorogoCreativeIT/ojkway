package id.ojkway.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import id.ojkway.R;

public class DefaultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default);
    }
}
