package id.ojkway.ui.main;

import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;


import com.afollestad.materialdialogs.MaterialDialog;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.flaviofaria.kenburnsview.Transition;
import com.jaouan.revealator.Revealator;
import com.nineoldandroids.animation.Animator;
import com.tt.whorlviewlibrary.WhorlView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.utils.ServiceHandler;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;

public class SplashScreenActivity extends AppCompatActivity implements KenBurnsView.TransitionListener {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.logo2)
    ImageView logo2;
    @BindView(R.id.viewSwitcher)
    ViewSwitcher mViewSwitcher ;
    @BindView(R.id.img1)
    KenBurnsView img1;
    @BindView(R.id.whorl2)
    WhorlView whorl2;
    @BindView(R.id.the_awesome_view)
    FrameLayout the_awesome_view;
    @BindView(R.id.the_awesome_view_two)
    FrameLayout the_awesome_view_two;
    @BindView(R.id.the_awesome_view_three)
    FrameLayout the_awesome_view_three;
    @BindView(R.id.etUsername)
    MonseratEditText etUsername;
    @BindView(R.id.etPassword)
    MonseratEditText etPassword;

    private static final int TRANSITIONS_TO_SWITCH = 3;

    private int mTransitionsCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);
        img1.setTransitionListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                call();
            }
        }, 500);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 3000);
    }

    void call(){
        logo.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.BounceInUp)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        whorl2.setVisibility(View.VISIBLE);
                        whorl2.start();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                callthis();
                            }
                        }, 2000);

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .duration(500)
                .playOn(logo);
    }

    @Override
    public void onTransitionStart(Transition transition) {

    }


    @Override
    public void onTransitionEnd(Transition transition) {
        mTransitionsCount++;
        if (mTransitionsCount == TRANSITIONS_TO_SWITCH) {
            mViewSwitcher.showNext();
            mTransitionsCount = 0;
        }


    }

    void callthis(){
        Revealator.reveal(the_awesome_view_two)
                .from(whorl2)
                .withCurvedTranslation()
                .withRevealDuration(1000)
                .withTranslateDuration(200)
                .start();
        Revealator.reveal(the_awesome_view)
                .withCurvedTranslation()
                .withRevealDuration(1000)
                .withTranslateDuration(200)
                //.withCurvedTranslation(curvePoint)
                //.withChildsAnimation()
                //.withDelayBetweenChildAnimation(...)
                //.withChildAnimationDuration(...)
                //.withTranslateDuration(...)
                //.withHideFromViewAtTranslateInterpolatedTime(...)
                //.withRevealDuration(...)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        //TranslateAnimation animation = new TranslateAnimation(0, 0, 0, 200);
                        //TranslateAnimation animation = new TranslateAnimation(10,0,20,0);
                        //animation.setDuration(1000);
                        //animation.setFillAfter(false);
                        //animation.setAnimationListener(new MyAnimationListener());
                        whorl2.animate().translationY(0);

                    }
                })
                .start();
        logo.animate()
                .translationY(0)
                .setDuration(100)
                .alpha(0.0f)
                .setListener(new android.animation.Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(android.animation.Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(android.animation.Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(android.animation.Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(android.animation.Animator animation) {

                    }
                });
        Revealator.reveal(the_awesome_view_three)
                .withCurvedTranslation()
                .withRevealDuration(1000)
                .withTranslateDuration(200)
                .start();
        /*
        Animation test = new ResizeAnimation(logo, logo.getWidth(), logo.getHeight(), logo.getWidth() - 200, logo.getHeight() - 200);
        logo.startAnimation(test);
        test.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                logo.clearAnimation();
                logo.getLayoutParams().width = logo.getWidth() - 200;
                logo.getLayoutParams().height = logo.getWidth() - 200;

                TranslateAnimation aa = new TranslateAnimation(0, 0, 0, -700);
                aa.setDuration(300);
                aa.setFillAfter(true);
                aa.setAnimationListener(new MyAnimationListener());

                logo.startAnimation(aa);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        */
    }

    private class MyAnimationListener implements Animation.AnimationListener {

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }

    }

    public class ResizeAnimation extends Animation {
        private View mView;
        private float mToHeight;
        private float mFromHeight;

        private float mToWidth;
        private float mFromWidth;

        public ResizeAnimation(View v, float fromWidth, float fromHeight, float toWidth, float toHeight) {
            mToHeight = toHeight;
            mToWidth = toWidth;
            mFromHeight = fromHeight;
            mFromWidth = fromWidth;
            mView = v;
            setDuration(300);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            float height =
                    (mToHeight - mFromHeight) * interpolatedTime + mFromHeight;
            float width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
            ViewGroup.LayoutParams p = mView.getLayoutParams();
            p.height = (int) height;
            p.width = (int) width;
            mView.requestLayout();
        }
    }

    @OnClick(R.id.btnLogin)
    public void btnLogin(){
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        int error = 0;
        if(username.equals("")){
            YoYo.with(Techniques.Shake)
                    .duration(700)
                    .playOn(etUsername);
            error++;
            Toast.makeText(SplashScreenActivity.this, "Field username tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }

        if(password.equals("")){
            YoYo.with(Techniques.Shake)
                    .duration(700)
                    .playOn(etPassword);
            error++;
            Toast.makeText(SplashScreenActivity.this, "Field password tidak boleh kosong", Toast.LENGTH_SHORT).show();
        }

        if(error == 0){
            try {
                dataPostJSon.put("username", username);
                dataPostJSon.put("password", password);
            }catch (JSONException e){

            }

            new auth().execute();
        }
    }

    JSONObject dataPostJSon = new JSONObject();
    String StatusServer;
    String StatusAuth;
    String token, firstname, lastname, email;
    String message;
    MaterialDialog mDialog;
    private class auth extends AsyncTask<Void, Void, Void> {
        public String url = ApplicationConstants.SERVER_API + "api/auth";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            StatusServer = "";
            mDialog = utils.showDialog(SplashScreenActivity.this, "Please wait");
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            ServiceHandler sh = new ServiceHandler();
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            //params.add(new BasicNameValuePair("id", id));

            String jsonStr = sh.makeServiceCall(SplashScreenActivity.this, url, ServiceHandler.POSTJSON, dataPostJSon);

            Log.d("url : ", url);
            Log.d("Response: ", "> " + jsonStr);

            if (jsonStr != null) {
                StatusServer = "OK";
                try {
                    String[] data = jsonStr.split("<link");
                    JSONObject jsonObj = new JSONObject(data[0]);
                    StatusAuth = jsonObj.getString("ststus");
                    if(StatusAuth.equals("success")){
                        token = jsonObj.getJSONObject("data").getString("token");
                        firstname = jsonObj.getJSONObject("data").getJSONObject("user").getString("first_name");
                        lastname = jsonObj.getJSONObject("data").getJSONObject("user").getString("last_name");
                        email = jsonObj.getJSONObject("data").getJSONObject("user").getString("email");
                    } else if (StatusAuth.equals("error")) {
                        message = jsonObj.getString("message");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                StatusServer = "FAIL";
                Log.e("ServiceHandler", "Couldn't get any data from the url  ");
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mDialog.dismiss();
            if(StatusServer.equals("OK")){
                if(StatusAuth.equals("success")) {
                    Toast.makeText(SplashScreenActivity.this, "Login Berhasil", Toast.LENGTH_SHORT).show();
                }else if(StatusAuth.equals("error")){
                    Toast.makeText(SplashScreenActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }else{
                Toast.makeText(SplashScreenActivity.this, "Pastikan koneksi internet tersedia", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
