package id.ojkway.ui.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.afollestad.materialdialogs.MaterialDialog;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.daimajia.easing.linear.Linear;
import com.flaviofaria.kenburnsview.KenBurnsView;
import com.flaviofaria.kenburnsview.Transition;
import com.jaouan.revealator.Revealator;
import com.nineoldandroids.animation.Animator;
import com.tt.whorlviewlibrary.WhorlView;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.ui.member.HomeActivity;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratEditText;

public class SplashScreenNewActivity extends AppCompatActivity  implements KenBurnsView.TransitionListener {

    @BindView(R.id.logo)
    ImageView logo;
    @BindView(R.id.viewSwitcher)
    ViewSwitcher mViewSwitcher ;
    @BindView(R.id.img1)
    KenBurnsView img1;
    @BindView(R.id.logo2)
    ImageView logo2;
    @BindView(R.id.loading_view)
    WhorlView loading_view;
    @BindView(R.id.layout_login)
    LinearLayout layout_login;
    @BindView(R.id.layout_logo_atas)
    LinearLayout layout_logo_atas;
    @BindView(R.id.btnLogin)
    View btnLogin;
    @BindView(R.id.etUsername)
    MonseratEditText etUsername;
    @BindView(R.id.etPassword)
    MonseratEditText etPassword;

    private static final int TRANSITIONS_TO_SWITCH = 3;

    private int mTransitionsCount = 0;

    @Override
    public void onTransitionStart(Transition transition) {

    }


    @Override
    public void onTransitionEnd(Transition transition) {
        mTransitionsCount++;
        if (mTransitionsCount == TRANSITIONS_TO_SWITCH) {
            mViewSwitcher.showNext();
            mTransitionsCount = 0;
        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreennew);
        ButterKnife.bind(this);

        AndroidNetworking.initialize(this);
        img1.setTransitionListener(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                call();
            }
        }, 1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

            }
        }, 3000);
    }

    void call(){

        YoYo.with(Techniques.BounceInUp)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        logo.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        loading_view.setVisibility(View.VISIBLE);
                        loading_view.start();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if(utils.getDataSession(SplashScreenNewActivity.this, "sessionID") == null){
                                    callthis();
                                }else{
                                    finish();
                                    startActivity(new Intent(SplashScreenNewActivity.this, HomeActivity.class));
                                }

                            }
                        }, 2000);

                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .duration(500)
                .playOn(logo);
    }

    void callthis(){
       /* Revealator.reveal(btnLogin)
                .from(loading_view)
                .withCurvedTranslation()
                .withRevealDuration(1000)
                .withTranslateDuration(200)
                .start();*/
        Revealator.reveal(layout_login)
                .from(loading_view)
                .withCurvedTranslation()
                .withRevealDuration(1000)
                .withTranslateDuration(200)
                //.withCurvedTranslation(curvePoint)
                //.withChildsAnimation()
                //.withDelayBetweenChildAnimation(...)
                //.withChildAnimationDuration(...)
                //.withTranslateDuration(...)
                //.withHideFromViewAtTranslateInterpolatedTime(...)
                //.withRevealDuration(...)
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        //TranslateAnimation animation = new TranslateAnimation(0, 0, 0, 200);
                        //TranslateAnimation animation = new TranslateAnimation(10,0,20,0);
                        //animation.setDuration(1000);
                        //animation.setFillAfter(false);
                        //animation.setAnimationListener(new MyAnimationListener());
                        loading_view.animate().translationY(0);

                    }
                })
                .start();
        logo.animate()
                .translationY(0)
                .setDuration(100)
                .alpha(0.0f)
                .setListener(new android.animation.Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(android.animation.Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(android.animation.Animator animation) {

                    }

                    @Override
                    public void onAnimationCancel(android.animation.Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(android.animation.Animator animation) {

                    }
                });
        Revealator.reveal(layout_logo_atas)
                .withCurvedTranslation()
                .withRevealDuration(1000)
                .withTranslateDuration(200)
                .start();
        /*
        Animation test = new ResizeAnimation(logo, logo.getWidth(), logo.getHeight(), logo.getWidth() - 200, logo.getHeight() - 200);
        logo.startAnimation(test);
        test.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                logo.clearAnimation();
                logo.getLayoutParams().width = logo.getWidth() - 200;
                logo.getLayoutParams().height = logo.getWidth() - 200;

                TranslateAnimation aa = new TranslateAnimation(0, 0, 0, -700);
                aa.setDuration(300);
                aa.setFillAfter(true);
                aa.setAnimationListener(new MyAnimationListener());

                logo.startAnimation(aa);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        */
    }

    String username = "", password = "";
    @OnClick(R.id.btnLogin)
    public void btnLogin(){
        error = 0;
        username = etUsername.getText().toString();
        checkField(etUsername, "Field username tidak boleh kosong");
        password = etPassword.getText().toString();
        checkField(etPassword, "Field password tidak boleh kosong");

        if(error == 0){
            postlogin();
        }
    }

    void postlogin(){
        final MaterialDialog mDialog = utils.showDialog(this, "Please wait");
        AndroidNetworking.post(ApplicationConstants.SERVER_API + "api/v1/login")
                .addBodyParameter("username", username)
                .addBodyParameter("password", password)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mDialog.dismiss();
                        try{
                            if(response.getString("status").equals("true")){
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionID", response.getJSONObject("data").getString("id"));
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionNAME", response.getJSONObject("data").getString("full_name"));
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionEMAIL", response.getJSONObject("data").getString("email"));
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionUSERNAME", response.getJSONObject("data").getString("username"));
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionPASSWORD", password);
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionSATKER", response.getJSONObject("data").getString("nm_unit_kerja"));
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionTOKEN", response.getString("token"));
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionPHOTOPROFILE", response.getJSONObject("data").getString("photo_profile").replace(" ", "%20"));
                                utils.setDataSession(SplashScreenNewActivity.this, "sessionHEADER", response.getJSONObject("data").getString("cover_photo").replace(" ", "%20"));
                                finish();
                                startActivity(new Intent(SplashScreenNewActivity.this, HomeActivity.class));
                            }else if(response.getString("status").equals("false")){
                                Toast.makeText(SplashScreenNewActivity.this, response.getString("pesan"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){

                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        mDialog.dismiss();
                    }
                });
    }

    int error;

    void checkField(EditText et, String sti){
        if(et.getText().toString().equals("")){
            error++;
            et.setError(sti);
            et.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    etUsername.setError(null);
                }
            });
        }
    }
}
