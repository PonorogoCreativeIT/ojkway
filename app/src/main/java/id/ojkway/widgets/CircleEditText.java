package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class CircleEditText extends EditText {
    public CircleEditText(Context context) {
        super(context, null);
        setTypeface(app.circle);
    }

    public CircleEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.circle);
    }

    public CircleEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.circle);
    }

}
