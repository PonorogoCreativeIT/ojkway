package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RadioButton;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class CicleBoldRadioButton extends RadioButton {
    public CicleBoldRadioButton(Context context) {
        super(context, null);
        setTypeface(app.circeBold);
    }

    public CicleBoldRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.circeBold);
    }

    public CicleBoldRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.circeBold);
    }

}
