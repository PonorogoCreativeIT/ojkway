package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class MonseratTextView extends TextView {
    public MonseratTextView(Context context) {
        super(context, null);
        setTypeface(app.monserat);
    }

    public MonseratTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.monserat);
    }

    public MonseratTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.monserat);
    }

}
