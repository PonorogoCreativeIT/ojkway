package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

import com.twotoasters.jazzylistview.JazzyListView;

/**
 * Created by PonorogoCreativeIT on 6/29/16.
 */
public class WrapContentJazzyListView extends JazzyListView {

    public WrapContentJazzyListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WrapContentJazzyListView(Context context) {
        super(context);
    }

    public WrapContentJazzyListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,
                MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);
    }

}