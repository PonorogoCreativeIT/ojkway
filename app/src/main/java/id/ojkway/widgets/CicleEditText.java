package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class CicleEditText extends EditText {
    public CicleEditText(Context context) {
        super(context, null);
        setTypeface(app.cicle);
    }

    public CicleEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.cicle);
    }

    public CicleEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.cicle);
    }

}
