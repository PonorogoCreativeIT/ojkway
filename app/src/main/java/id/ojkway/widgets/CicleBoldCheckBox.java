package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class CicleBoldCheckBox extends CheckBox {
    public CicleBoldCheckBox(Context context) {
        super(context, null);
        setTypeface(app.circeBold);
    }

    public CicleBoldCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.circeBold);
    }

    public CicleBoldCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.circeBold);
    }

}
