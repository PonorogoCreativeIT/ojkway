package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class MonseratEditText extends EditText {
    public MonseratEditText(Context context) {
        super(context, null);
        setTypeface(app.monserat);
    }

    public MonseratEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.monserat);
    }

    public MonseratEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.monserat);
    }

}
