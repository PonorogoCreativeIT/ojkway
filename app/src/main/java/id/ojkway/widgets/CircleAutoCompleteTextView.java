package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class CircleAutoCompleteTextView extends AutoCompleteTextView {
    public CircleAutoCompleteTextView(Context context) {
        super(context, null);
        setTypeface(app.circle);
    }

    public CircleAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.circle);
    }

    public CircleAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.circle);
    }

}
