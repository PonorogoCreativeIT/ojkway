package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class CicleAutoCompleteTextView extends AutoCompleteTextView {
    public CicleAutoCompleteTextView(Context context) {
        super(context, null);
        setTypeface(app.cicle);
    }

    public CicleAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.cicle);
    }

    public CicleAutoCompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.cicle);
    }

}
