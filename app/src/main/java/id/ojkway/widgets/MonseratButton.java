package id.ojkway.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

import id.ojkway.utils.app;

/**
 * Created by Dmytro Denysenko on 5/6/15.
 */
public class MonseratButton extends Button {
    public MonseratButton(Context context) {
        super(context, null);
        setTypeface(app.monserat);
    }

    public MonseratButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(app.monserat);
    }

    public MonseratButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setTypeface(app.monserat);
    }

}
