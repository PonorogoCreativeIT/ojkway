package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.R;
import id.ojkway.model.DashboardModel;
import id.ojkway.model.MessageModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by Acer on 5/16/2016.
 */
public class MessageAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<MessageModel> listData;
    Activity activity;

    public MessageAdapter(Activity mainActivity, List<MessageModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        LinearLayout layout;
        CircleImageView viewProfile;
        MonseratTextView viewName;
        MonseratTextView viewMessage;
        MonseratTextView viewTime;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final MessageModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        TypedValue tv = new TypedValue();

        int actionBarHeight = 0;
        if (context.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,context.getResources().getDisplayMetrics());
        }


        rowView = inflater.inflate(R.layout.item_message, null);
        holder.layout=(LinearLayout) rowView.findViewById(R.id.layout);
        holder.viewProfile=(CircleImageView) rowView.findViewById(R.id.viewProfile);
        holder.viewName=(MonseratTextView) rowView.findViewById(R.id.viewName);
        holder.viewMessage=(MonseratTextView) rowView.findViewById(R.id.viewMessage);
        holder.viewTime=(MonseratTextView) rowView.findViewById(R.id.viewTime);

        Picasso.with(activity)
                .load(data.getphoto())
                .into(holder.viewProfile);

        holder.viewName.setText(data.getnama());
        holder.viewMessage.setText(data.getmessage());
        holder.viewTime.setText(utils.getTimeAgo(data.gettanggal()));

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        if(position == 0){
            params.setMargins(0, actionBarHeight, 0, 0);
            holder.layout.setLayoutParams(params);
        }

        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/



        return rowView;
    }

}
