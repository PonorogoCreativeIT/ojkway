package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.R;
import id.ojkway.model.ChatModel;
import id.ojkway.model.MessageModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by Acer on 5/16/2016.
 */
public class ChatAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<ChatModel> listData;
    Activity activity;

    public ChatAdapter(Activity mainActivity, List<ChatModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        LinearLayout layout;
        LinearLayout layout_kiri;
        LinearLayout layout_kanan;
        CircleImageView viewProfileKiri;
        MonseratTextView viewMessageKiri;
        CircleImageView viewProfileKanan;
        MonseratTextView viewMessageKanan;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ChatModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        TypedValue tv = new TypedValue();

        int actionBarHeight = 0;
        if (context.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true))
        {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,context.getResources().getDisplayMetrics());
        }


        rowView = inflater.inflate(R.layout.item_chat, null);
        holder.layout_kiri=(LinearLayout) rowView.findViewById(R.id.layout_kiri);
        holder.layout_kanan=(LinearLayout) rowView.findViewById(R.id.layout_kanan);
        holder.layout=(LinearLayout) rowView.findViewById(R.id.layout);
        holder.viewProfileKiri=(CircleImageView) rowView.findViewById(R.id.viewProfileKiri);
        holder.viewMessageKiri=(MonseratTextView) rowView.findViewById(R.id.viewMessageKiri);
        holder.viewProfileKanan=(CircleImageView) rowView.findViewById(R.id.viewProfileKanan);
        holder.viewMessageKanan=(MonseratTextView) rowView.findViewById(R.id.viewMessageKanan);



        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        if(position == 0){
            params.setMargins(0, actionBarHeight, 0, 0);
            holder.layout.setLayoutParams(params);
        }
        String id = utils.getDataSession(activity, "sessionID");
        if(data.getid().equals(id)){
            holder.layout_kiri.setVisibility(View.GONE);
            holder.layout_kanan.setVisibility(View.VISIBLE);
            Picasso.with(activity)
                    .load(data.getprofile())
                    .into(holder.viewProfileKanan);
            holder.viewMessageKanan.setText(data.getmessage());
        }else{
            holder.layout_kiri.setVisibility(View.VISIBLE);
            holder.layout_kanan.setVisibility(View.GONE);
            Picasso.with(activity)
                    .load(data.getprofile())
                    .into(holder.viewProfileKiri);
            holder.viewMessageKiri.setText(data.getmessage());
        }

        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/



        return rowView;
    }

}
