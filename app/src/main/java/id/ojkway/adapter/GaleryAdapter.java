package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.R;
import id.ojkway.model.GaleryModel;
import id.ojkway.model.MessageModel;
import id.ojkway.widgets.MonseratTextView;

import static com.bumptech.glide.load.engine.DiskCacheStrategy.ALL;

/**
 * Created by Acer on 5/16/2016.
 */
public class GaleryAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<GaleryModel> listData;
    Activity activity;

    private final Random mRandom;


    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public GaleryAdapter(Activity mainActivity, List<GaleryModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        mRandom = new Random();
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        LinearLayout layouts;
        ImageView viewImage;
        MonseratTextView viewText;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final GaleryModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;




        rowView = inflater.inflate(R.layout.item_galery, null);
        holder.layouts=(LinearLayout) rowView.findViewById(R.id.layouts);
        holder.viewImage=(ImageView) rowView.findViewById(R.id.viewImage);
        holder.viewText=(MonseratTextView) rowView.findViewById(R.id.viewText);



        Glide.with(activity)
                .load(data.getimage())
                .diskCacheStrategy(ALL)
                .into(holder.viewImage);

        /*Picasso.with(activity)
                .load(data.getimage())
                .into(holder.viewImage);
        */
        holder.viewText.setText(data.gettext());



        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/



        return rowView;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
            //Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }


}
