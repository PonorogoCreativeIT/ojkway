package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.model.FJBCategoryDetailModel;
import id.ojkway.model.FJBCategoryModel;
import id.ojkway.ui.member.DetailFJBActivity;
import id.ojkway.ui.member.FJBDetailCategoryActivity;
import id.ojkway.utils.utils;
import id.ojkway.widgets.IconFontAwesome;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by Acer on 5/16/2016.
 */
public class FJBCategoryDetailAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<FJBCategoryDetailModel> listData;
    Activity activity;

    public FJBCategoryDetailAdapter(Activity mainActivity, List<FJBCategoryDetailModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        ImageView imageFJB;
        MonseratTextView judulFJB;
        MonseratTextView hargaFJB;
        View detailFJB;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final FJBCategoryDetailModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_fjbdetailcategory, null);
        holder.imageFJB = (ImageView) rowView.findViewById(R.id.imageFJB);
        holder.judulFJB=(MonseratTextView) rowView.findViewById(R.id.judulFJB);
        holder.hargaFJB=(MonseratTextView) rowView.findViewById(R.id.hargaFJB);
        holder.detailFJB=(View) rowView.findViewById(R.id.detailFJB);

        //Log.v("test", "-" +  utils.getDataSession(activity, data.geticon()));
        //holder.viewIcon.setText(Html.fromHtml(utils.getDataSession(activity, data.geticon())));
        Log.v("url Img ", ApplicationConstants.SERVER_ASSETS + "images/commerce/" + data.getimage());
        Picasso.with(activity)
                .load(ApplicationConstants.SERVER_ASSETS + "images/commerce/" + data.getimage())
                .into(holder.imageFJB);
        holder.judulFJB.setText(data.getnama());
        holder.hargaFJB.setText(utils.setCurrency(data.getharga()));
        holder.detailFJB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FJBCategoryDetailModel getData = listData.get(position);
                Bundle data = new Bundle();
                data.putString("id", getData.getid());
                data.putString("nama", getData.getnama());
                activity.startActivity(new Intent(activity, DetailFJBActivity.class).putExtras(data));
            }
        });

        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/


        if(position != 0){
            Animation animation = null;
            animation = AnimationUtils.loadAnimation(activity, R.anim.push_left_in);
            animation.setDuration(200);
            rowView.startAnimation(animation);
            animation = null;
        }
        return rowView;
    }

}
