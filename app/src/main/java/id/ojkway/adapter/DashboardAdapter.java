package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.model.DashboardModel;
import id.ojkway.ui.member.CommentActivity;
import id.ojkway.utils.utils;
import id.ojkway.widgets.MonseratTextView;
import io.realm.Realm;

/**
 * Created by Acer on 5/16/2016.
 */
public class DashboardAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<DashboardModel> listData;
    Activity activity;

    public DashboardAdapter(Activity mainActivity, List<DashboardModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        View btnDelete;
        View layout;
        CircleImageView viewProfile;
        ImageView viewImage;
        MonseratTextView viewNama;
        MonseratTextView viewTanggal;
        MonseratTextView viewDeskripsi;
        MonseratTextView viewLike;
        MonseratTextView viewComment;
        LikeButton like;
        LinearLayout layoutAtas;
        LinearLayout layoutTanggal;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final DashboardModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_dashboard, null);
        holder.btnDelete = (View) rowView.findViewById(R.id.btnDelete);
        holder.layout = (View) rowView.findViewById(R.id.layout);
        holder.viewProfile=(CircleImageView) rowView.findViewById(R.id.viewProfile);
        holder.viewImage=(ImageView) rowView.findViewById(R.id.viewImage);
        holder.viewNama=(MonseratTextView) rowView.findViewById(R.id.viewNama);
        holder.viewTanggal=(MonseratTextView) rowView.findViewById(R.id.viewTanggal);
        holder.viewDeskripsi=(MonseratTextView) rowView.findViewById(R.id.viewDeskripsi);
        holder.viewLike=(MonseratTextView) rowView.findViewById(R.id.viewLike);
        holder.viewComment=(MonseratTextView) rowView.findViewById(R.id.viewComment);
        holder.like=(LikeButton) rowView.findViewById(R.id.like);
        holder.layoutAtas=(LinearLayout) rowView.findViewById(R.id.layoutAtas);
        holder.layoutTanggal=(LinearLayout) rowView.findViewById(R.id.layoutTanggal);

        Glide.with(activity)
                .load(ApplicationConstants.SERVER_ASSETS  + data.getimageprofile())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.viewProfile);

        if(data.getliked().equals("0")){
            holder.like.setLiked(false);
        }else{
            holder.like.setLiked(true);
        }

        if(data.getimage().equals("") || data.getimage().equals("null")){
            holder.viewImage.getLayoutParams().height = 60;


            LinearLayout.LayoutParams layoutParam = new LinearLayout.LayoutParams(
                    utils.getDP(activity, 50), utils.getDP(activity, 50));
            layoutParam.setMargins(utils.getDP(activity, 10), utils.getDP(activity, 10), 0, 0);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
            layoutParams.setMargins(utils.getDP(activity, 10), utils.getDP(activity, 10), 0, 0);

            LinearLayout.LayoutParams layoutParamss = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParamss.setMargins(0, utils.getDP(activity, 10), 0, 0);

            holder.viewNama.setLayoutParams(layoutParams);
            holder.viewProfile.setLayoutParams(layoutParam);
            holder.layoutTanggal.setLayoutParams(layoutParamss);
            holder.layoutAtas.setGravity(Gravity.CENTER_VERTICAL);
        }else{
            holder.viewImage.setVisibility(View.VISIBLE);
            Glide.with(activity)
                    .load(ApplicationConstants.SERVER_ASSETS + "images/user_status/" + data.getimage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.viewImage);
        }
        holder.viewNama.setText(data.getnama());
        holder.viewTanggal.setText(data.gettime());
        holder.viewDeskripsi.setText(data.getdeskripsi());
        holder.viewLike.setText(data.gettotalLike() + " like");
        holder.viewComment.setText(data.gettotalComment() + " comment");


        holder.like.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {

            }

            @Override
            public void unLiked(LikeButton likeButton) {

            }
        });
        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/


        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            YoYo.with(Techniques.SlideOutLeft)
                .duration(1000)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        holder.layout.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .playOn(holder.layout);
            }
        });

        if(position != 0){
            Animation animation = null;
            animation = AnimationUtils.loadAnimation(activity, R.anim.push_left_in);
            animation.setDuration(200);
            rowView.startAnimation(animation);
            animation = null;
        }

        holder.viewComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle dataIntent = new Bundle();
                dataIntent.putString("id", data.getid());
                activity.startActivity(new Intent(activity, CommentActivity.class).putExtras(dataIntent));
            }
        });
        return rowView;
    }

    void like(String id){
    }
}
