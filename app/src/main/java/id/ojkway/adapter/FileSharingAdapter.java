package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;

import java.util.List;

import id.ojkway.R;
import id.ojkway.model.FJBCategoryModel;
import id.ojkway.model.FileSharingModel;
import id.ojkway.utils.utils;
import id.ojkway.widgets.IconFontAwesome;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by Acer on 5/16/2016.
 */
public class FileSharingAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<FileSharingModel> listData;
    Activity activity;

    public FileSharingAdapter(Activity mainActivity, List<FileSharingModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        IconFontAwesome viewIcon;
        MonseratTextView viewName;
        MonseratTextView viewDeskripsi;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final FileSharingModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_filesharing, null);
        holder.viewIcon = (IconFontAwesome) rowView.findViewById(R.id.viewIcon);
        holder.viewName=(MonseratTextView) rowView.findViewById(R.id.viewName);
        holder.viewDeskripsi=(MonseratTextView) rowView.findViewById(R.id.viewDeskripsi);

        Log.v("test", "-" +  utils.getDataSession(activity, data.geticon()));
        holder.viewIcon.setText(Html.fromHtml(utils.getDataSession(activity, data.geticon())));
        holder.viewName.setText(data.getnama());
        //holder.viewDeskripsi.setText(data.getdeskripsi());

        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/


        if(position != 0){
            Animation animation = null;
            animation = AnimationUtils.loadAnimation(activity, R.anim.push_left_in);
            animation.setDuration(200);
            rowView.startAnimation(animation);
            animation = null;
        }
        return rowView;
    }

}
