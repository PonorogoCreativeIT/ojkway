package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.model.CommentFJBModel;
import id.ojkway.model.JawabanAdkModel;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by Acer on 5/16/2016.
 */
public class JawabanAdkAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<JawabanAdkModel> listData;
    Activity activity;

    public JawabanAdkAdapter(Activity mainActivity, List<JawabanAdkModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        CircleImageView profile;
        MonseratTextView nama;
        MonseratTextView tannggal;
        MonseratTextView komentar;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final JawabanAdkModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_comment, null);
        holder.profile=(CircleImageView) rowView.findViewById(R.id.profile);
        holder.nama=(MonseratTextView) rowView.findViewById(R.id.nama);
        holder.tannggal=(MonseratTextView) rowView.findViewById(R.id.tannggal);
        holder.komentar=(MonseratTextView) rowView.findViewById(R.id.komentar);

        Picasso.with(activity)
                .load(ApplicationConstants.SERVER_ASSETS + data.getimageprofile())
                .into(holder.profile);

        holder.nama.setText(data.getnama());
        holder.tannggal.setText(data.gettanggal());
        holder.komentar.setText(data.getcomment());


        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/





        return rowView;
    }

}
