package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Random;

import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.model.AddImageModel;

/**
 * Created by Acer on 5/16/2016.
 */
public class AddImageAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<AddImageModel> listData;
    Activity activity;

    private final Random mRandom;


    private static final SparseArray<Double> sPositionHeightRatios = new SparseArray<Double>();

    public AddImageAdapter(Activity mainActivity, List<AddImageModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        mRandom = new Random();
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        ImageView imageFJB;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final AddImageModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;



        rowView = inflater.inflate(R.layout.item_add_fjb, null);
        holder.imageFJB=(ImageView) rowView.findViewById(R.id.imageFJB);

        if(data.gettype().equals("url")){
            Picasso.with(activity)
                    .load(ApplicationConstants.SERVER_ASSETS + "images/commerce/" + data.getimage())
                    .into(holder.imageFJB);
        }else if(data.gettype().equals("string")){
            byte[] decodedString = Base64.decode(data.getimage(), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            holder.imageFJB.setImageBitmap(decodedByte);
        }



        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/



        return rowView;
    }

    private double getPositionRatio(final int position) {
        double ratio = sPositionHeightRatios.get(position, 0.0);
        // if not yet done generate and stash the columns height
        // in our real world scenario this will be determined by
        // some match based on the known height and width of the image
        // and maybe a helpful way to get the column height!
        if (ratio == 0) {
            ratio = getRandomHeightRatio();
            sPositionHeightRatios.append(position, ratio);
            //Log.d(TAG, "getPositionRatio:" + position + " ratio:" + ratio);
        }
        return ratio;
    }

    private double getRandomHeightRatio() {
        return (mRandom.nextDouble() / 2.0) + 1.0; // height will be 1.0 - 1.5 the width
    }


}
