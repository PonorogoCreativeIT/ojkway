package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.model.DetailAdkModel;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by Acer on 5/16/2016.
 */
public class TiketSayaAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<DetailAdkModel> listData;
    Activity activity;

    public TiketSayaAdapter(Activity mainActivity, List<DetailAdkModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        CircleImageView profile;
        MonseratTextView viewName;
        MonseratTextView viewDeskripsi;
        LinearLayout layout;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final DetailAdkModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_detailadk, null);
        holder.profile = (CircleImageView) rowView.findViewById(R.id.profile);
        holder.viewName=(MonseratTextView) rowView.findViewById(R.id.viewName);
        holder.viewDeskripsi=(MonseratTextView) rowView.findViewById(R.id.viewDeskripsi);
        holder.layout=(LinearLayout) rowView.findViewById(R.id.layout);

        //Log.v("test", "-" +  utils.getDataSession(activity, data.geticon()));
        //holder.viewIcon.setText(Html.fromHtml(utils.getDataSession(activity, data.geticon())));


        holder.viewName.setText(data.getnama());
        //holder.viewIcon.setText(Html.fromHtml(utils.getDataSession(activity, data.getimage())));
        holder.viewDeskripsi.setText(data.gettext());

        Picasso.with(activity)
                .load(ApplicationConstants.SERVER_ASSETS + data.getimage())
                .into(holder.profile);

        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/


        if(position != 0){
            Animation animation = null;
            animation = AnimationUtils.loadAnimation(activity, R.anim.push_left_in);
            animation.setDuration(200);
            rowView.startAnimation(animation);
            animation = null;
        }
        return rowView;
    }

}
