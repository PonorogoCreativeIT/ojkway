package id.ojkway.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.R;
import id.ojkway.model.DashboardModel;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by ponorogocreativeit on 21/09/16.
 */

public class DashboardAdapterr extends RecyclerView.Adapter<DashboardAdapterr.ViewHolderRoute> {
    private Context context;
    private List<DashboardModel> lisData;

    public DashboardAdapterr(RecyclerView rv,Context context, List<DashboardModel> lisData) {
        this.context = context;
        this.lisData = lisData;
    }

    @Override
    public ViewHolderRoute onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_dashboard, parent, false);
        return new ViewHolderRoute(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolderRoute holder, final int position) {
        DashboardModel data = lisData.get(position);
        //String upperString = data.getairport_name().substring(0, 1).toUpperCase() + data.getairport_name().substring(1);
        Picasso.with(context)
                .load(data.getimageprofile())
                .into(holder.viewProfile);
        Picasso.with(context)
                .load(data.getimage())
                .into(holder.viewImage);

        holder.viewNama.setText(data.getnama());
        holder.viewTanggal.setText(data.gettime());
        holder.viewDeskripsi.setText(data.getdeskripsi());
        holder.viewLike.setText(data.gettotalLike() + " like");
        holder.viewComment.setText(data.gettotalComment() + " comment");

        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*//*
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);

        holder.layout.startAnimation(animation);*/

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoYo.with(Techniques.SlideOutLeft)
                        .duration(1000)
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                holder.layout.setVisibility(View.GONE);
                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        })
                        .playOn(holder.layout);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lisData.size();
    }

    public static class ViewHolderRoute extends RecyclerView.ViewHolder {
        @BindView(R.id.viewProfile)
        CircleImageView viewProfile;
        @BindView(R.id.viewImage)
        ImageView viewImage;
        @BindView(R.id.viewNama)
        MonseratTextView viewNama;
        @BindView(R.id.viewTanggal)
        MonseratTextView viewTanggal;
        @BindView(R.id.viewDeskripsi)
        MonseratTextView viewDeskripsi;
        @BindView(R.id.viewLike)
        MonseratTextView viewLike;
        @BindView(R.id.viewComment)
        MonseratTextView viewComment;
        @BindView(R.id.layout)
        View layout;
        @BindView(R.id.btnDelete)
        View btnDelete;

        public ViewHolderRoute(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}