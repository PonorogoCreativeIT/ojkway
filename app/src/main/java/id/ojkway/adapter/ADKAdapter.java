package id.ojkway.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import id.ojkway.ApplicationConstants;
import id.ojkway.R;
import id.ojkway.model.ADKModel;
import id.ojkway.model.FileSharingModel;
import id.ojkway.ui.member.DetailADKActivity;
import id.ojkway.utils.utils;
import id.ojkway.widgets.IconFontAwesome;
import id.ojkway.widgets.MonseratTextView;

/**
 * Created by Acer on 5/16/2016.
 */
public class ADKAdapter extends BaseAdapter {
    Context context;
    private static LayoutInflater inflater=null;
    private List<ADKModel> listData;
    Activity activity;

    public ADKAdapter(Activity mainActivity, List<ADKModel> listData) {
        // TODO Auto-generated constructor stub
        this.activity = mainActivity;
        this.context=mainActivity;
        this.listData = listData;
        this.inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        animationStates = new boolean[listData.size()];
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        ImageView viewProfile;
        MonseratTextView viewName;
        MonseratTextView viewDeskripsi;
        View btnGiveQuestion;
        LinearLayout layout;
    }
    boolean[] animationStates;
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final ADKModel data = listData.get(position);
        final Holder holder=new Holder();
        View rowView;

        rowView = inflater.inflate(R.layout.item_adk, null);
        holder.viewProfile = (ImageView) rowView.findViewById(R.id.viewProfile);
        holder.viewName=(MonseratTextView) rowView.findViewById(R.id.viewName);
        holder.viewDeskripsi=(MonseratTextView) rowView.findViewById(R.id.viewDeskripsi);
        holder.btnGiveQuestion=(View) rowView.findViewById(R.id.btnGiveQuestion);

        Log.v("test", "-" +  utils.getDataSession(activity, data.getnama()));
        //holder.viewIcon.setText(Html.fromHtml(utils.getDataSession(activity, data.geticon())));
        holder.viewName.setText(data.getnama());

        Picasso.with(activity)
                .load(ApplicationConstants.SERVER_ASSETS + data.getimage())
                .into(holder.viewProfile);
        holder.viewDeskripsi.setText(data.getdeskripsi());

        /*YoYo.with(Techniques.SlideInLeft)
                .duration(1000)
                .playOn(holder.layout);*/
            //Log.e("TAG", "Animating item no: " + position);
        /*animationStates[position] = true;
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fadein);
        animation.setStartOffset(100);
        rowView.startAnimation(animation);*/

        holder.btnGiveQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle setDataIntent = new Bundle();
                setDataIntent.putString("id", data.getid());
                setDataIntent.putString("nama", data.getnama());
                setDataIntent.putString("deskripsi", data.getdeskripsi());
                setDataIntent.putString("image", data.getimage());
                activity.startActivity(new Intent(activity, DetailADKActivity.class).putExtras(setDataIntent));
            }
        });

        if(position != 0){
            Animation animation = null;
            animation = AnimationUtils.loadAnimation(activity, R.anim.push_left_in);
            animation.setDuration(200);
            rowView.startAnimation(animation);
            animation = null;
        }
        return rowView;
    }

}
