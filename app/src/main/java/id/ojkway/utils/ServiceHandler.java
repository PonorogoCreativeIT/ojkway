package id.ojkway.utils;

import android.app.Activity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ServiceHandler {

    static String response = null;
    public final static int GET = 1;
    public final static int POST = 2;
    public final static int POSTJSON = 3;
    public final static int DELETEJSON = 3;

    public ServiceHandler() {

    }

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * */
    public String makeServiceCall(Activity activity, String url, int method, JSONObject data) {
        return this.makeServiceCall(activity, url, method, null, data);
    }

    public String makeServiceCall(Activity activity, String url, int method) {
        return this.makeServiceCall(activity, url, method, null);
    }

    /**
     * Making service call
     * @url - url to make request
     * @method - http request method
     * @params - http request params
     * */
    public String makeServiceCall(Activity activity, String url, int method,
                                  List<NameValuePair> params, JSONObject data) {
        try {
            // http client
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpEntity httpEntity = null;
            HttpResponse httpResponse = null;

            // Checking http request method type
            if (method == POST) {
                HttpPost httpPost = new HttpPost(url);
                httpPost.addHeader("X-Oc-Merchant-Id","085334353789");
                httpPost.addHeader("X-Oc-Session",utils.getDataSession(activity, "SessionServer"));
                // adding post params
                if (params != null) {
                    httpPost.setEntity(new UrlEncodedFormEntity(params));
                }

                httpResponse = httpClient.execute(httpPost);

            } else if (method == GET) {
                // appending params to url
                if (params != null) {
                    String paramString = URLEncodedUtils
                            .format(params, "utf-8");
                    url += "?" + paramString;
                }
                HttpGet httpGet = new HttpGet(url);
                httpGet.addHeader("X-Oc-Merchant-Id","085334353789");
                httpGet.addHeader("X-Oc-Session",utils.getDataSession(activity, "SessionServer"));

                httpResponse = httpClient.execute(httpGet);

            }else if( method ==  POSTJSON){
                HttpPost request = new HttpPost(url);
                request.addHeader("content-type", "application/json");
                request.setEntity(new StringEntity(data.toString()));
                httpResponse = httpClient.execute(request);
            }else if( method ==  DELETEJSON){
                URL urlS = new URL(url);
                HttpURLConnection urlConnection = (HttpURLConnection) urlS.openConnection();
                try {
                    urlConnection.setRequestMethod("DELETE");
                    urlConnection.setDoOutput(true);
                    // Maybe you should set the request properties here too, but it depends on your body.

                    OutputStreamWriter osw = new OutputStreamWriter(urlConnection.getOutputStream());
                    osw.write(data.toString());
                    osw.flush();

                    int HttpResult = urlConnection.getResponseCode();
                    if(HttpResult == HttpURLConnection.HTTP_OK) {
                        InputStream is = new BufferedInputStream(urlConnection.getInputStream());
                        // Handle the response.
                        response = is.toString();
                    } else {
                        // ...
                    }
                }
                finally {
                    urlConnection.disconnect();
                }
            }
            httpEntity = httpResponse.getEntity();
            response = EntityUtils.toString(httpEntity);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }

}