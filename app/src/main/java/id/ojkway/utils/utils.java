package id.ojkway.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import id.ojkway.R;

/**
 * Created by Acer on 4/6/2016.
 */
public class utils {
    public static void fullscreenAndShowStatusBar(Activity activity){
        activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @SuppressLint("NewApi")
    public static Bitmap blurRenderScript(Context context,Bitmap smallBitmap, int radius) {
        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }

    private static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static void setMarginStatusBar(Context context, View toolbar){
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) toolbar.getLayoutParams();
        params.setMargins(0, utils.getStatusBarHeight(context), 0, 0);
        toolbar.setLayoutParams(params);
    }

    public static void setMarginToolbar(Context context, View toolbar){
        ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) toolbar.getLayoutParams();
        params.setMargins(0, utils.getToolBarHeight(context), 0, 0);
        toolbar.setLayoutParams(params);
    }

    public  static int getToolBarHeight(Context context){
        TypedValue tv = new TypedValue();
        int actionBarHeight = 0;
        if (context.getTheme().resolveAttribute(R.attr.actionBarSize, tv, true))
        {
             actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,context.getResources().getDisplayMetrics());
        }
        return actionBarHeight;
    }

    public static void logLargeString(String tag, String str) {
        if(str.length() > 3000) {
            Log.i(tag, str.substring(0, 3000));
            logLargeString(tag, str.substring(3000));
        } else {
            Log.i(tag, str); // continuation
        }
    }

    public static void setDataSession(Context context,String jenis, String value){
        SharedPreferences PonorogoCreativeIT_at_gmail_dot_com_app =  context.getSharedPreferences("PonorogoCreativeIT_at_gmail_dot_com_app", Context.MODE_PRIVATE);
        SharedPreferences.Editor PonorogoCreativeIT_at_gmail_dot_com_app_Editor = PonorogoCreativeIT_at_gmail_dot_com_app.edit();
        PonorogoCreativeIT_at_gmail_dot_com_app_Editor.putString(jenis, value);
        PonorogoCreativeIT_at_gmail_dot_com_app_Editor.commit();
    }

    public static String getDataSession(Context context,String jenis){
        SharedPreferences PonorogoCreativeIT_at_gmail_dot_com_app =  context.getSharedPreferences("PonorogoCreativeIT_at_gmail_dot_com_app", Context.MODE_PRIVATE);
        return PonorogoCreativeIT_at_gmail_dot_com_app.getString(jenis, null);
    }

    public static void hidePDialog(ProgressDialog pDialog) {
        if (pDialog != null) {
            pDialog.dismiss();
            pDialog = null;
        }
    }

    public static void showPDialog(Context context, ProgressDialog pDialog){
        if(pDialog == null){
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();
        }
    }

    public static String setCurrency(String data){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###", symbols);
        String result = decimalFormat.format(Integer.valueOf(data));

        return "Rp " + result;
    }

    public static String setCurrencyWithOutRp(String data){
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        DecimalFormat decimalFormat = new DecimalFormat("###,###,###,###", symbols);
        String result = decimalFormat.format(Integer.valueOf(data));

        return result;
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }



    private static View mChildOfContent;
    private static int usableHeightPrevious;
    private static  FrameLayout.LayoutParams frameLayoutParams;

    public static void AndroidBug5497Workaround(Activity activity) {
        FrameLayout content = (FrameLayout) activity.findViewById(android.R.id.content);
        mChildOfContent = content.getChildAt(0);
        mChildOfContent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                possiblyResizeChildOfContent();
            }
        });
        frameLayoutParams = (FrameLayout.LayoutParams) mChildOfContent.getLayoutParams();
    }

    public static void possiblyResizeChildOfContent() {
        int usableHeightNow = computeUsableHeight();
        if (usableHeightNow != usableHeightPrevious) {
            int usableHeightSansKeyboard = mChildOfContent.getRootView().getHeight();
            int heightDifference = usableHeightSansKeyboard - usableHeightNow;
            if (heightDifference > (usableHeightSansKeyboard/4)) {
                // keyboard probably just became visible
                frameLayoutParams.height = usableHeightSansKeyboard - heightDifference;
            } else {
                // keyboard probably just became hidden
                frameLayoutParams.height = usableHeightSansKeyboard;
            }
            mChildOfContent.requestLayout();
            usableHeightPrevious = usableHeightNow;
        }
    }

    public static int computeUsableHeight() {
        Rect r = new Rect();
        mChildOfContent.getWindowVisibleDisplayFrame(r);
        return (r.bottom);
    }

    public static String ConvertDate(String time){
        String departureDate ="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = format.parse(time);

            format = new SimpleDateFormat("d LLLL yyyy");
            departureDate = format.format(newDate);;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static String ConvertDateyyyyMMdd(String time){
        String departureDate ="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = format.parse(time);

            format = new SimpleDateFormat("d LLLL yyyy");
            departureDate = format.format(newDate);;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static String ConvertDatedLLLLyyyy(String time){
        String departureDate ="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("d LLLL yyyy");
            Date newDate = format.parse(time);

            format = new SimpleDateFormat("yyyy-MM-dd");
            departureDate = format.format(newDate);;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static String ConvertDateToDay(String time){
        String departureDate ="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("d LLLL yyyy");
            Date newDate = format.parse(time);

            format = new SimpleDateFormat("dd");
            departureDate = format.format(newDate);;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static String ConvertDateToMonth(String time){
        String departureDate ="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("d LLLL yyyy");
            Date newDate = format.parse(time);

            format = new SimpleDateFormat("MM");
            departureDate = format.format(newDate);;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static String ConvertDateToYear(String time){
        String departureDate ="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("d LLLL yyyy");
            Date newDate = format.parse(time);

            format = new SimpleDateFormat("yyyy");
            departureDate = format.format(newDate);;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static String ConvertDateToHHMM(String time){
        String departureDate ="";
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date newDate = format.parse(time);

            format = new SimpleDateFormat("HH:mm");
            departureDate = format.format(newDate);;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static Long getTimeMilisFromHoursAndMinutes(String time){
        Long departureDate = 0L;
        try {
            departureDate = new SimpleDateFormat("HH:mm", Locale.US).parse(time).getTime();
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static String getTimeMilis(){
        Long tsLong = System.currentTimeMillis()/1000;
        return tsLong.toString();
    }

    public static String getTimeMilisFull(){
        Long tsLong = System.currentTimeMillis();
        return tsLong.toString();
    }

    public static String getTimeNow(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(c.getTime());
    }

    public static String getStringFullname(String text){
        String result = "";
        String tem = text.replace("  ", " ").replace("   ", " ").replace("    ", " ").replace("     ", " ").replace("      ", " ").replace("       ", " ");
        String[] cekText = tem.split(" ");

        if(cekText.length > 1){
            String a = String.valueOf(cekText[0].charAt(0));
            String b = String.valueOf(cekText[1].charAt(0));
            result = String.valueOf(a) + String.valueOf(b);
        }else{
            String a = String.valueOf(cekText[0].charAt(0));
            String b = String.valueOf(cekText[0].charAt(1));
            result = String.valueOf(a) + String.valueOf(b);
        }

        return result.toUpperCase();
    }

    public static MaterialDialog showDialog(Context context, String pleasewait){
        final MaterialDialog dialog = new MaterialDialog.Builder(context)
                .customView(R.layout.dialog_loading, true)
                .canceledOnTouchOutside(false)
                .show();

        final View viewDialog = dialog.getCustomView();
        ((TextView) viewDialog.findViewById(R.id.txtPleaseWait)).setText(pleasewait);
        return  dialog;
    }

    public static String getTimeHoursAndMinutesNow(){
        Calendar cal = Calendar.getInstance();
        Date currentLocalTime = cal.getTime();
        DateFormat date = new SimpleDateFormat("HH:mm");
        return date.format(currentLocalTime);
    }

    public static String hariSekarang(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String today = "";
        switch (day) {
            case Calendar.SUNDAY:
                today = "Minggu";
                return today;
            case Calendar.MONDAY:
                today = "Senin";
                return today;
            case Calendar.TUESDAY:
                today = "Selasa";
                return today;
            case Calendar.WEDNESDAY:
                today = "Rabu";
                return today;
            case Calendar.THURSDAY:
                today = "Kamis";
                return today;
            case Calendar.FRIDAY:
                today = "Jum'at";
                return today;
            case Calendar.SATURDAY:
                today = "Sabtu";
                return today;
        }

        return today;
    }

    private static final int SECOND_MILLIS = 1;
    private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
    private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
    private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

    public static long converToTimeMilis(String time){
        Long departureDate = 0L;
        try {
            departureDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(time).getTime() / 1000;
        }catch (ParseException e){

        }
        return  departureDate;
    }

    public static int getDP(Activity act, int num){
        final float scale = act.getResources().getDisplayMetrics().density;
        int pixels = (int) (num * scale + 0.5f);

        return pixels;
    }
    public static String getTimeAgo(String timems) {
        long time = utils.converToTimeMilis(timems);
        long now = Long.valueOf(utils.getTimeMilis());

        // TODO: localize
        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return "just now";
        } else if (diff < 2 * MINUTE_MILLIS) {
            return "a minute ago";
        } else if (diff < 50 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " minutes ago";
        } else if (diff < 90 * MINUTE_MILLIS) {
            return "an hour ago";
        } else if (diff < 24 * HOUR_MILLIS) {
            return diff / HOUR_MILLIS + " hours ago";
        } else if (diff < 48 * HOUR_MILLIS) {
            return "yesterday";
        } else {
            return diff / DAY_MILLIS + " days ago";
        }
        /*if (diff < 60 * MINUTE_MILLIS) {
            return diff / MINUTE_MILLIS + " menit yang lalu";
        }else {*/
            /*String departureDate ="";
            try {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date newDate = format.parse(timems);

                format = new SimpleDateFormat("d LLLL yyyy HH:mm");
                departureDate = format.format(newDate);;
            }catch (ParseException e){

            }
            return  departureDate;*/
        /*}*/
    }

    public static boolean isNetworkAvailable(final Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    public static String cutWords(String data){
        String[] trimData = data.split(" ");
        String tempData = "";

        for(int i = 0; i < trimData.length; i++){
            if(i < 8){
                tempData += trimData[i] + " ";
            }
        }

        tempData += "...";

        return tempData;
    }

    public static void setEditTextAsCurrency(final EditText et){
        et.addTextChangedListener(new TextWatcher() {
            private String current = "";

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals("")) {
                    et.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[$,.]", "");

                    if (cleanString.length() > 0) {
                        double parsed = Double.parseDouble(cleanString);
                        DecimalFormat formatter = new DecimalFormat("#,###,###");
                        current = formatter.format(parsed).replace(",",".");
                    } else {
                        current = cleanString;
                    }


                    et.setText(current);
                    et.setSelection(current.length());
                    et.addTextChangedListener(this);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
        });
    }

}
