package id.ojkway.utils;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;
import android.util.Log;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Acer on 5/15/2016.
 */
public class app extends Application {

    private static app instance;
    private static final int TIMEOUT_MS = 30000; // 50second
    private static final String monseratPath = "fonts/monserat.otf";
    public static Typeface monserat;
    private static final String ciclePath = "fonts/cicle_gordita.otf";
    public static Typeface cicle;
    private static final String circlePath = "fonts/circe_light.otf";
    public static Typeface circle;
    private static final String circeBoldPath = "fonts/circe_bold.otf";
    public static Typeface circeBold;
    private static final String fontawesomepath = "fonts/fontawesome-webfont.ttf";
    public static Typeface fontawesome;

    @Override
    public void onCreate()
    {
        super.onCreate();
        callRealm();
        instance = this;
        initTypefacemonserat();
        initTypefaceCicle();
        initTypefaceCircle();
        initTypefaceCirceBold();
        initTypefaceFontAwesome();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public void callRealm(){
        RealmConfiguration config = new RealmConfiguration.Builder(this).build();
        Realm.setDefaultConfiguration(config);
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void initTypefacemonserat() {
        monserat = Typeface.createFromAsset(getAssets(), monseratPath);

    }

    private void initTypefaceCicle() {
        cicle = Typeface.createFromAsset(getAssets(), ciclePath);

    }

    private void initTypefaceCircle() {
        circle = Typeface.createFromAsset(getAssets(), circlePath);

    }

    private void initTypefaceCirceBold() {
        circeBold = Typeface.createFromAsset(getAssets(), circeBoldPath);

    }

    private void initTypefaceFontAwesome() {
        fontawesome = Typeface.createFromAsset(getAssets(), fontawesomepath);

    }
}
