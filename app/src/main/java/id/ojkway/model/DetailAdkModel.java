package id.ojkway.model;

/**
 * Created by Admin on 8/17/2016.
 */
public class DetailAdkModel {
    private String id;
    private String nama;
    private String image;
    private String text;


    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String gettext() {
        return this.text;
    }

    public void settext(String text) {
        this.text = text;
    }

    public String getimage() {
        return this.image;
    }

    public void setimage(String image) {
        this.image = image;
    }

}
