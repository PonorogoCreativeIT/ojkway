package id.ojkway.model;

/**
 * Created by Admin on 11/2/2016.
 */
import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

// [START post_class]
@IgnoreExtraProperties
public class ChatModelNew {

    public String text;
    public String email;
    public String date;
    public String profile_picture;
    public String username;
    public String id;

    public ChatModelNew() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public ChatModelNew(String text, String email, String date, String profile_picture, String username, String id) {
        this.text = text;
        this.email = email;
        this.date = date;
        this.profile_picture = profile_picture;
        this.username = username;
        this.id = id;
    }

    // [START post_to_map]
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("text", text);
        result.put("email", email);
        result.put("date", date);
        result.put("profile_picture", profile_picture);
        result.put("username", username);
        result.put("id", id);

        return result;
    }
    // [END post_to_map]

}