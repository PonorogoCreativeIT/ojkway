package id.ojkway.model;

/**
 * Created by Admin on 10/25/2016.
 */

public class JawabanAdkModel {
    private String id;
    private String imageprofile;
    private String nama;
    private String tanggal;
    private String comment;

    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getimageprofile() {
        return this.imageprofile;
    }

    public void setimageprofile(String imageprofile) {
        this.imageprofile = imageprofile;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String gettanggal() {
        return this.tanggal;
    }

    public void settanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getcomment() {
        return this.comment;
    }

    public void setcomment(String comment) {
        this.comment = comment;
    }
}
