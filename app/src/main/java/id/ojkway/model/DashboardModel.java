package id.ojkway.model;

/**
 * Created by Admin on 8/17/2016.
 */
public class DashboardModel {
    private String id;
    private String nama;
    private String time;
    private String imageprofile;
    private String image;
    private String deskripsi;
    private String totalLike;
    private String totalComment;
    private String liked;

    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String gettime() {
        return this.time;
    }

    public void settime(String time) {
        this.time = time;
    }

    public String getimageprofile() {
        return this.imageprofile;
    }

    public void setimageprofile(String imageprofile) {
        this.imageprofile = imageprofile;
    }

    public String getimage() {
        return this.image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    public String getdeskripsi() {
        return this.deskripsi;
    }

    public void setdeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String gettotalLike() {
        return this.totalLike;
    }

    public void settotalLike(String totalLike) {
        this.totalLike = totalLike;
    }

    public String gettotalComment() {
        return this.totalComment;
    }

    public void settotalComment(String totalComment) {
        this.totalComment = totalComment;
    }

    public String getliked() {
        return this.liked;
    }

    public void setliked(String liked) {
        this.liked = liked;
    }
}
