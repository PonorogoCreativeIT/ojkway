package id.ojkway.model;

/**
 * Created by ponorogocreativeit on 24/09/16.
 */

public class MessageModel {

    private String id;
    private String photo;
    private String nama;
    private String message;
    private String tanggal;

    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getphoto() {
        return this.photo;
    }

    public void setphoto(String photo) {
        this.photo = photo;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String getmessage() {
        return this.message;
    }

    public void setmessage(String message) {
        this.message = message;
    }

    public String gettanggal() {
        return this.tanggal;
    }

    public void settanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
