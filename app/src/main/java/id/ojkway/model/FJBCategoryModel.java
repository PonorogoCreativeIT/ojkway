package id.ojkway.model;

/**
 * Created by Admin on 8/17/2016.
 */
public class FJBCategoryModel {
    private String id;
    private String nama;
    private String deskripsi;
    private String icon;


    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String getdeskripsi() {
        return this.deskripsi;
    }

    public void setdeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String geticon() {
        return this.icon;
    }

    public void seticon(String icon) {
        this.icon = icon;
    }




}
