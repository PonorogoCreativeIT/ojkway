package id.ojkway.model;

/**
 * Created by ponorogocreativeit on 24/09/16.
 */

public class AddImageModel {

    private String type;
    private String image;
    private String url;

    public String gettype() {
        return this.type;
    }

    public void settype(String type) {
        this.type = type;
    }

    public String getimage() {
        return this.image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    public String geturl() {
        return this.url;
    }

    public void seturl(String url) {
        this.url = url;
    }
}
