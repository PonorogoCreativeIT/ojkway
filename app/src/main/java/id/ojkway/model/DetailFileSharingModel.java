package id.ojkway.model;

/**
 * Created by Admin on 8/17/2016.
 */
public class DetailFileSharingModel {
    private String id;
    private String nama;
    private String harga;
    private String image;
    private String url;
    private String deskripsi;


    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String getharga() {
        return this.harga;
    }

    public void setharga(String harga) {
        this.harga = harga;
    }

    public String getimage() {
        return this.image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    public String geturl() {
        return this.url;
    }

    public void seturl(String url) {
        this.url = url;
    }

    public String getdeskripsi() {
        return this.deskripsi;
    }

    public void setdeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

}
