package id.ojkway.model;

/**
 * Created by ponorogocreativeit on 24/09/16.
 */

public class ChatModel {

    private String id;
    private String nama;
    private String profile;
    private String message;

    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String getprofile() {
        return this.profile;
    }

    public void setprofile(String profile) {
        this.profile = profile;
    }

    public String getmessage() {
        return this.message;
    }

    public void setmessage(String message) {
        this.message = message;
    }
}
