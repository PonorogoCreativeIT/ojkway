package id.ojkway.model;

/**
 * Created by ponorogocreativeit on 24/09/16.
 */

public class MajalahModel {

    private String id;
    private String image;
    private String text;
    private String url;

    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getimage() {
        return this.image;
    }

    public void setimage(String image) {
        this.image = image;
    }

    public String gettext() {
        return this.text;
    }

    public void settext(String text) {
        this.text = text;
    }

    public String geturl() {
        return this.url;
    }

    public void seturl(String url) {
        this.url = url;
    }
}
