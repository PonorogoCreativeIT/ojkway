package id.ojkway.model;

/**
 * Created by Admin on 8/17/2016.
 */
public class ADKModel {
    private String id;
    private String nama;
    private String deskripsi;
    private String image;


    public String getid() {
        return this.id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getnama() {
        return this.nama;
    }

    public void setnama(String nama) {
        this.nama = nama;
    }

    public String getdeskripsi() {
        return this.deskripsi;
    }

    public void setdeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getimage() {
        return this.image;
    }

    public void setimage(String image) {
        this.image = image;
    }




}
